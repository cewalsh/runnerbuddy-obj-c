RunnerBuddy iOS

This component of the RunnerBuddy system runs on iOS devices.

This component of the RunnerBuddy system is the IOS version of the mobile
application of RunnerBuddy.  The function of this component is the same as the
RunnerBuddyAndroid component, monitor current gps location, monitor nearby
BLE devices, signal the spectator when a 'buddy' is nearby, and upload all the
data about location and 'seen' BLE devices to the RunnerBuddy data store.

RunnerBuddy iOS uses the RunnerBuddyRest RESTful web services to communicate 
with the RunnerBuddy data store.  It does not connect to the data store directly.