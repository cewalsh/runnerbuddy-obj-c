//
//  RunClassTests.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 2/4/16.
//  Copyright © 2016 ProObject. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Run.h"
#import "Run_private.h"

@interface RunClassTests : XCTestCase {

@private
    //These guys get init-ed with proper files and dates.
    Run *publicTestRun_properInit;
    Run_private *privateTestRun_properInit;
    
    
    /*The improper ones below deal with improper format.*/
    
    //Thse guys get init-ed with improper file, but proper dates.
    Run *publicTestRun_improperFileInit;
    Run_private *privateTestRun_improperFileInit;
    
    //These guys get init-ed with improper date, but proper files.
    Run *publicTestRun_improperDateInit;
    Run_private *privateTestRun_improperDateInit;
    
    //These guys get init-ed with both improper date and files.
    Run *publicTestRun_improperBothInit;
    Run_private *privateTestRun_improperBothInit;
    
    
    /*The improper ones below deal with improper values.*/
    //Thse guys get init-ed with improper file, but proper dates.
    Run *publicTestRun_improperFileInit_V;
    Run_private *privateTestRun_improperFileInit_V;
    
    //These guys get init-ed with improper date, but proper files.
    Run *publicTestRun_improperDateInit_V;
    Run_private *privateTestRun_improperDateInit_V;
    
    //These guys get init-ed with both improper date and files.
    Run *publicTestRun_improperBothInit_V;
    Run_private *privateTestRun_improperBothInit_V;
    
    
    
    //Strings for the inits.
    
    NSString *goodFileName;
    NSString *goodDate;
    NSString *badFileName_F;
    NSString *badDate_F;
    NSString *badFileName_V;
    NSString *badDate_V;
    NSString *owner;
    
}
@end

@implementation RunClassTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    owner = @"tester!";
    
    goodFileName = @"/upload/runnerInfo/mumboCombo/web_head:2016_02_05:00_00_00.txt";
    goodDate = @"2016_02_05";
    
    badFileName_F = @"/upload/runnerInfo/mumboCombo/web_head:00_00_00.txt";
    badDate_F = @"2016_02";
    
    badFileName_V = @"/upload/runnerInfo/mumboCombo/web_head:2017_02_05:00_00_00.txt";
    badDate_V = @"2017_02_05";
    
    
    //Init the proper ones.
    publicTestRun_properInit = [[Run alloc] initWithFileName:goodFileName andDate:goodDate];
    publicTestRun_properInit.owner = owner;
    privateTestRun_properInit = (Run_private *)publicTestRun_properInit;
    
    //Init the improper file one <format>
    publicTestRun_improperFileInit = [[Run alloc] initWithFileName:badFileName_F andDate:goodDate];
    publicTestRun_improperFileInit.owner = owner;
    privateTestRun_improperFileInit = (Run_private *)publicTestRun_improperFileInit;
    
    //Init the improper date one <format>
    publicTestRun_improperDateInit = [[Run alloc] initWithFileName:goodFileName andDate:badDate_F];
    publicTestRun_improperDateInit.owner = owner;
    privateTestRun_improperDateInit = (Run_private *)publicTestRun_improperDateInit;
    
    //Init improper both <format>
    publicTestRun_improperBothInit = [[Run alloc] initWithFileName:badFileName_F andDate:badDate_F];
    publicTestRun_improperBothInit.owner = owner;
    privateTestRun_improperBothInit = (Run_private *)publicTestRun_improperBothInit;
    
    
    /***************/
    /***************/
    /***************/
    /***************/
    
    //Init the improper file one <value>
    publicTestRun_improperFileInit_V = [[Run alloc] initWithFileName:badFileName_V andDate:goodDate];
    publicTestRun_improperFileInit_V.owner = owner;
    privateTestRun_improperFileInit_V = (Run_private *)publicTestRun_improperFileInit_V;
    
    //Init the improper date one <value>
    publicTestRun_improperDateInit_V = [[Run alloc] initWithFileName:goodFileName andDate:badDate_V];
    publicTestRun_improperDateInit_V.owner = owner;
    privateTestRun_improperDateInit_V = (Run_private *)publicTestRun_improperDateInit_V;
    
    //Init improper both <value>
    publicTestRun_improperBothInit_V = [[Run alloc] initWithFileName:badFileName_V andDate:badDate_V];
    publicTestRun_improperBothInit_V.owner = owner;
    privateTestRun_improperBothInit_V = (Run_private *)publicTestRun_improperBothInit_V;
    
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


//Tests whether the right dates were pushed through all of the Runs.
-(void)testDates{
    //Proper date init
    XCTAssertTrue( [publicTestRun_properInit.date isEqualToString:goodDate] ,@"Strings are not equal %@ %@", publicTestRun_properInit.date, goodDate );
    
    //Improper date format.
    XCTAssertTrue( [publicTestRun_improperDateInit.date isEqualToString:badDate_F] ,@"Strings are not equal %@ %@", publicTestRun_improperDateInit.date , badDate_F );
    
    //Improper fileName format.
    
    XCTAssertTrue( [publicTestRun_improperFileInit.date isEqualToString:goodDate] ,@"Strings are not equal %@ %@", publicTestRun_improperFileInit.date, goodDate );
    
    //Both Improper.
    XCTAssertTrue( [publicTestRun_improperBothInit.date isEqualToString:badDate_F] ,@"Strings are not equal %@ %@", publicTestRun_improperBothInit.date, badDate_F );
    
    //Improper file value
    XCTAssertTrue( [publicTestRun_improperFileInit_V.date isEqualToString:goodDate] ,@"Strings are not equal %@ %@", publicTestRun_improperFileInit_V.date, goodDate );
    
    //Improper date value
    XCTAssertTrue( [publicTestRun_improperDateInit_V.date isEqualToString:badDate_V] ,@"Strings are not equal %@ %@", publicTestRun_improperDateInit_V.date, badDate_V );
    
    //improper both value
    XCTAssertTrue( [publicTestRun_improperBothInit_V.date isEqualToString:badDate_V] ,@"Strings are not equal %@ %@", publicTestRun_improperBothInit_V.date, badDate_V );
}


//Tests whether the right file names were pushed through all of the Runs.
-(void)testFileNames{
    //Proper date init
    XCTAssertTrue( [ [privateTestRun_properInit getFileName] isEqualToString:goodFileName] ,@"Strings are not equal %@ %@", [privateTestRun_properInit getFileName], goodFileName );
    
    //Improper date format.
    XCTAssertTrue( [ [privateTestRun_improperDateInit getFileName] isEqualToString:goodFileName] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName] , goodFileName );
    
    //Improper fileName format.
    
    XCTAssertTrue( [[privateTestRun_improperFileInit getFileName] isEqualToString:badFileName_F] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName], badFileName_F );
    
    //Both Improper.
    XCTAssertTrue( [[privateTestRun_improperBothInit getFileName] isEqualToString:badFileName_F] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName], badFileName_F );
    
    //Improper file value
    XCTAssertTrue( [[privateTestRun_improperFileInit_V getFileName] isEqualToString:badFileName_V] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName], badFileName_V );
    
    //Improper date value
    XCTAssertTrue( [[privateTestRun_improperDateInit_V getFileName] isEqualToString:goodFileName] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName], goodFileName );
    
    //improper both value
    XCTAssertTrue( [[privateTestRun_improperBothInit_V getFileName] isEqualToString:badFileName_V] ,@"Strings are not equal %@ %@", [privateTestRun_improperDateInit getFileName], badFileName_V );
}



-(void)testFetchRunData{
    NSString *success = @"Success";
    NSString *fileNotFound = @"File does not exist";
    NSError *error_properInit;
    NSError *error_improperFile;
    NSError *error_improperDate;
    NSError *error_bothImproper;
    NSError *error_improperFile_V;
    NSError *error_improperDate_V;
    NSError *error_bothImproper_V;
    
    [privateTestRun_properInit fetchRunData:&error_properInit];
    XCTAssertTrue( [[error_properInit localizedDescription] isEqualToString:success] ,@"Strings are not equal %@ %@", [error_properInit localizedDescription], success  );
    
    [privateTestRun_improperFileInit fetchRunData:&error_improperFile];
    XCTAssertTrue( [[error_improperFile localizedDescription] isEqualToString:fileNotFound] ,@"Strings are not equal %@ %@", [error_improperFile localizedDescription], fileNotFound  );
    
    [privateTestRun_improperDateInit fetchRunData:&error_improperDate];
    XCTAssertTrue( [[error_improperDate localizedDescription] isEqualToString:success] ,@"Strings are not equal %@ %@", [error_improperDate localizedDescription], success  );
    
    [privateTestRun_improperBothInit fetchRunData:&error_bothImproper];
    XCTAssertTrue( [[error_bothImproper localizedDescription] isEqualToString:fileNotFound] ,@"Strings are not equal %@ %@", [error_bothImproper localizedDescription], fileNotFound  );
    
    [privateTestRun_improperFileInit_V fetchRunData:&error_improperFile_V];
    XCTAssertTrue( [[error_improperFile_V localizedDescription] isEqualToString:fileNotFound] ,@"Strings are not equal %@ %@", [error_improperFile_V localizedDescription], fileNotFound  );
    
    [privateTestRun_improperDateInit_V fetchRunData:&error_improperDate_V];
    XCTAssertTrue( [[error_improperDate_V localizedDescription] isEqualToString:success] ,@"Strings are not equal %@ %@", [error_improperDate_V localizedDescription], success  );
    
    [privateTestRun_improperBothInit_V fetchRunData:&error_bothImproper_V];
    XCTAssertTrue( [[error_bothImproper_V localizedDescription] isEqualToString:fileNotFound] ,@"Strings are not equal %@ %@", [error_bothImproper_V localizedDescription], fileNotFound  );
    
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
