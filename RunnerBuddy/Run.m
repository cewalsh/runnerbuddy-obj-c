//
//  Run.m

//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "Run.h"
#import "NetworkIO.h"
#import <MapKit/MapKit.h>
#import "BenjaminsPoint.h"
#import "Run_private.h"

@interface Run()


@property NSString *fileName;
@property NSMutableArray *currentPoints;
@property NSUInteger numOfMostRecentPoints;

@end

@interface Run_private()
@end

@implementation Run

-(id) initWithFileName:(NSString *) nameOfFile andDate:(NSString *) date{
    self = [super init];
    self.fileName = nameOfFile;
    self.date = date;
    self.currentPoints = [[NSMutableArray alloc] init];
    _numOfMostRecentPoints = 0;
    return self;
}

//Method fetchs the data for the run from the server, based in the fileName.
-(void) fetchRunData:(NSError**)error{
    
    //ToDo: Implement Methods from Zach Attack so that I can pull down the file.
    NSString *fileData = [NetworkIO getFileFrom:self.fileName errorCode:error];
    
    //ToDo: Organize the file pulled down (in the form a String right now)
    //      into either a) a class that I have created that can be passed to
    //                     the map interface, which (when consumed) is turned into the
    //                      the appropriate class for the Map
    //                  b) the class the the Map naturally consumes to create points on itself.
    
    [self convertFileData:fileData];
    
}



//Converts the fileData, then stores it in an array of those objects.
-(void)convertFileData: (NSString *)fileData{
    NSArray *runnerData = [fileData componentsSeparatedByString:@":"];
    
    //Little fancy play here. So, we only want to add the NEW entries.
    
    
    
    //grab the long, lat ༼ つ ◕_◕ ༽つ
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < runnerData.count; i++)
    {
        NSArray *runnerPoint = [[runnerData objectAtIndex:i] componentsSeparatedByString:@","];
        if (runnerPoint.count != 4)
        {
            continue; 
            //NSLog(@"The delimited runner point does not have 4 values (date/time, lat, long, user)");
        }
        NSArray *splitDateTime = [runnerPoint[0] componentsSeparatedByString:@" "];
        int temp;
        if ( [splitDateTime count] == 2){
            temp = 1;
        } else { temp = 2;}
        NSArray *timeSplit = [splitDateTime[temp] componentsSeparatedByString:@"_"];
        int hour = [timeSplit[0] intValue];
        NSMutableString *time;
        if (hour < 12){
            time = [NSMutableString stringWithFormat:@": %d:%@:%@AM", hour, timeSplit[1], timeSplit[2]];
        } else {
            if (hour != 12) hour-=12;
            time = [NSMutableString stringWithFormat:@": %d:%@:%@PM", hour, timeSplit[1], timeSplit[2]];
        }
        
        NSString *latitude = [runnerPoint objectAtIndex:1];
        NSString *longitude = [runnerPoint objectAtIndex:2];
        NSMutableString *userName = [[NSMutableString alloc] initWithString:_owner];
        CLLocationCoordinate2D coord;
        coord.latitude = latitude.doubleValue;
        coord.longitude = longitude.doubleValue;
        [userName appendString:time];
        // MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:title AndCoordinate:coord];
        
        BenjaminsPoint *point = [[BenjaminsPoint alloc] init];

        point.title = userName;
        point.coordinate = coord;
        point.subtitle = [NSString stringWithFormat:@"Point #%d", i];
        
        //point.
        point.time = [self getDateFromTitle:point.title];
        //point.image = [UIImage imageNamed:@"refresh.png"];
        
        [tempArray addObject:point];
    }
    
    //Sort all entries first.
    NSArray *sortedArray;
    sortedArray = [tempArray sortedArrayUsingSelector:@selector(compare:)];
    //Maybe it sorts the array, maybe it doesn't. Je ne sais pas, monsiuer. Et toi? Qu'est to peur faire? Ah! C'est la vie!
    
    //Assuming it sorted, we now compare the lengths of the currentPoints with that of the tempArray
    //And add only the new additions.
    
    //Given we may have many points, we should only call each of the count methods once,
    //well, the comparator one need only be execavated outside of the for loop contruct.
    NSUInteger size = [tempArray count];
    
    //this difference equals the number of new points.
    _numOfMostRecentPoints = size - [_currentPoints count];
    
    for(NSUInteger i = [self.currentPoints count]; i < size; i++){
        //Add temp points!
        [self.currentPoints addObject:tempArray[i]];
    }
    
    
}





//Method gets the date from the title of a point.
-(NSInteger)getDateFromTitle:(NSString *)title{
    
    //Okay, we can split title on ":" char.
    NSArray *splitTitle = [title componentsSeparatedByString:@":"];
    //index 1 contains our datetime.
   
    NSMutableString *combineTime = [[NSMutableString alloc] init];
    [combineTime appendString:splitTitle[1]]; //hour
    [combineTime appendString:splitTitle[2]]; //minutes
    [combineTime appendString:splitTitle[3]]; //seconds
    int value = [combineTime intValue];
    
    return value;
}


-(NSArray *)getMostRecent:(NSError**)error{
    [self fetchRunData:error];
    if ( [_currentPoints count] == _numOfMostRecentPoints){ return [_currentPoints copy];}
    
    return [_currentPoints subarrayWithRange:NSMakeRange(_numOfMostRecentPoints, [_currentPoints count] - _numOfMostRecentPoints)];
}

-(NSString *)getFileName{
    return _fileName;
}
-(NSMutableArray *)getCurrentPoints{
    return _currentPoints;
}
-(NSUInteger)getNumOfMostRecentPoints{
    return _numOfMostRecentPoints;
}


@end
