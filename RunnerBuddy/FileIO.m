//
//  FileIO.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/2/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "FileIO.h"

@implementation FileIO
+(void)saveFileToDevice:(NSMutableString *)fileName :(NSString*)data
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    [fileName appendString:@".csv"];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, fileName];
    NSLog(@"filePath %@", filePath);
    
//    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { // if file is not exist, create it.
        NSError *error;
        [data writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
//    }
    
    if ([[NSFileManager defaultManager] isWritableFileAtPath:filePath]) {
        NSLog(@"Writable");
    }else {
        NSLog(@"Not Writable");
    }
}
+(NSString*)getDataFromSavedFile:(NSString *)filePath
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *content = [NSString stringWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.csv",documentsDirectory,filePath] encoding:NSUTF8StringEncoding error:&error];
    return content;
}
+(NSMutableArray*)getSavedFilePaths
{
    NSMutableArray* filePaths = [[NSMutableArray alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    NSLog(@"files array %@", filePathsArray);
    
    for (int i=0; i < filePathsArray.count; i++)
    {
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[filePathsArray objectAtIndex:i]];
        [filePaths addObject:filePath];
    }
    return filePaths;
}


@end
