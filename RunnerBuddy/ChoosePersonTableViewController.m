//
//  ChoosePersonTableViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "ChoosePersonTableViewController.h"
#import "ToastView.h"
#import "ChooseDateTableViewController.h"
#import "Person.h"

@interface ChoosePersonTableViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingInd;
@property NSArray *listOfPeople;
@end

@implementation ChoosePersonTableViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _loadingInd.color = UIColor.darkGrayColor;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    //self.realDoneButton.action: @selector(doneButtonListener);
    
    //Okay, first things first, need to get the persons from the mapInterface instance.
    
    
    NSMutableArray *tempArray;
    if (!_useGoogleMaps) {
        tempArray = [self.mapInterface getCurrentPeeps];
    } else {
        tempArray = [self.gMapInterface getCurrentPeeps];
    }
    
    _listOfPeople = [tempArray sortedArrayUsingSelector:@selector(compare:)];
    

    
}

-(void)initLocalData{
    //self.listOfPeople = [[NSMutableArray alloc] init];
}

- (IBAction)cancelButtonListener:(id)sender {
    //Two options, either unwind to the options, or skip to map view.
    if (_source == 5){
        //Unwind to map view
        [self performSegueWithIdentifier:@"personToMapViewUnwind" sender:self];
    } else {
        [self performSegueWithIdentifier:@"personToOptionsUnwind" sender:self];
    }
    
    
}

- (IBAction)doneButtonListener:(id)sender {
 
    
    //Started by new following, so we head to the date part.
    if (self.source == 1){
        if ([self shouldPerformSegueWithIdentifier:@"newFollowingPath" sender:nil]){
            [self performSegueWithIdentifier: @"newFollowingPath" sender: self];
        }
    } else if (_source == 2){
        //Just changing the person, so head back.
        
        if (!_useGoogleMaps) {
            [self.mapInterface loadMap];
        } else {
            [self.gMapInterface loadMap];
        }
        [self performSegueWithIdentifier: @"personToOptionsUnwind" sender: self];
    } else {
        //Coming from main menu, still just go to the new date.
        if ([self shouldPerformSegueWithIdentifier:@"newFollowingPath" sender:nil]){
            [self performSegueWithIdentifier: @"newFollowingPath" sender: self];
        }
    }
    
   // [self toast: ((self.source == 1) ? @"from new Following!" : @"from new persons!")];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.listOfPeople count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListPrototypeCell" forIndexPath:indexPath];
    Person *person = (Person *)self.listOfPeople[indexPath.row];
    cell.textLabel.text = person.name;
    
    // Configure the cell...
    if(person.following){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    Person *tappedItem = [self.listOfPeople objectAtIndex:indexPath.row];
    tappedItem.following = !tappedItem.following;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"newFollowingPath"]){
        return [self anyoneChecked]; //This guy tries to load the map, if he fails he tells us. Nice guy.
    } else {
        return true;
    }
}

-(BOOL)anyoneChecked{
    int numPeeps = [_listOfPeople count];
    for(int i = 0; i < numPeeps; i++){
        if (((Person*)_listOfPeople[i]).following){
            //Check dates.
            if (!_useGoogleMaps) {
                if ( [[_mapInterface getDatesForPeepsChecked] count] == 0) {
                    [self displayMessage:@"None of those people have a common date." withTitle:@"Oops!"];
                    return false;
                }
            } else {
                if ( [[_gMapInterface getDatesForPeepsChecked] count] == 0) {
                    [self displayMessage:@"None of those people have a common date." withTitle:@"Oops!"];
                    return false;
                }
            }
            return true;
        }
    }
    [self displayMessage:@"You havn't choosen any people to follow, please choose someone or click Cancel if you don't wish to proceed." withTitle:@"Oops!"];
    return false;
}

//#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"newFollowingPath"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ChooseDateTableViewController *controller = (ChooseDateTableViewController *)navController.topViewController;
        if (!_useGoogleMaps) {
            controller.mapInterface = self.mapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.gMapInterface;
            controller.useGoogleMaps = true;
        }
        if (_source == 1) {
            controller.source = 4;
        } else if (_source == 5){
            controller.source = 6;
        }
    }
}


-(void)displayMessage: (NSString *)message withTitle:(NSString*)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
 
 
-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.view withText:message withDuaration:3.0];
}

@end
