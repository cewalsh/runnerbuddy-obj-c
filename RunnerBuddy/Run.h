//
//  Run.h
//      ~Represents a single run for a person. Contains the file name and any other data needed to create a pin point
//          on a map with regards to a run. (Caveat: Will only contain the data for the run IF the Person asked it too)
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Run : NSObject
@property NSString *date;
@property NSString *owner;
-(id) initWithFileName:(NSString *) nameOfFile andDate:(NSString *) date;
//-(NSMutableArray *) getRunData;
-(NSArray *)getMostRecent:(NSError**)error;


@end
