//
//  BenjaminsBeacon.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/23/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BenjaminsBeacon : NSObject
@property float latitude;
@property float longitude;
@property long rssi;
@property NSString *uuid;
@property NSString *timeCollected;
@end
