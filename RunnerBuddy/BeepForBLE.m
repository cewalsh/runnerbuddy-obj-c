//
//  BeepForBLE.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "BeepForBLE.h"
#import "PairingBeacon.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>
#import "VideoTriggeringService.h"

@interface BeepForBLE()
@property VideoTriggeringService *vts;
@property NSMutableArray *personalBeacons;
@property NSLock *pairedBeaconLock;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property BOOL soundOn;
@property BOOL videoTriggeringOn;
@property BOOL currentlyRecording;
@end

@implementation BeepForBLE

-(id)initWithBeacons:(NSMutableArray *)beaconsToListenFor andLock:(NSLock*) incLock{
    self = [super init];
    if (self) {
        executing = NO;
        finished = NO;
    }
    
    _soundOn = false;
    _videoTriggeringOn = false;
    _currentlyRecording = false;
    
    /*These are PairingBeacons*/
    
    _vts = [[VideoTriggeringService alloc] init];
    [_vts createSession];
    
    _pairedBeaconLock = incLock;
    _personalBeacons = beaconsToListenFor;
    
    
    
    
    NSError* error;
    NSURL *url = [NSURL URLWithString:@"/System/Library/Audio/UISounds/new-mail.caf"];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    return self;
}

-(void)start{
    if ([self isCancelled])
    {
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

-(void)forMainThread{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    [_locationManager requestAlwaysAuthorization];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    _myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.proobject.testregion"];
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
    
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

- (void)main {
    
    @try {
        
        /*Do Work.*/
        [self performSelectorOnMainThread:@selector(forMainThread) withObject:nil waitUntilDone:false];
        
        while (![self isCancelled]) {}
        
        [self completeOperation];
    }
    @catch(NSException *exception){
        //Catching anything that's pitched. Phrasing?
    }
    
    
}


-(void)completeOperation{
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
    [self stopVideoRecording];
    /*I have completed the operation. Now, reward me with doughnuts.*/
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}


-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region{
        /*Catch the beacons!*/
    
    long numOfIncBeacons = [beacons count];
    for (int i = 0; i < numOfIncBeacons; i++){
        PairingBeacon *pbTemp = [[PairingBeacon alloc] init];
        /*Get a temp, new beacons for comparison.*/
        CLBeacon *temp = (CLBeacon *)beacons[i];
        NSString *majorValue = [NSString stringWithFormat:@"%4lX",
                                (unsigned long)[temp.major integerValue]];
        NSString *minorValue =[NSString stringWithFormat:@"%4lX",
                               (unsigned long)[temp.minor integerValue]];
        NSString *uuid = [temp.proximityUUID.UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        minorValue = [minorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        majorValue = [majorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        
        
        pbTemp.uuid = [NSString stringWithFormat:@"%@%@%@", uuid,majorValue,minorValue];
        
        [_pairedBeaconLock  lock];
//        /*Now see if it matches any of the beacons I'm following...*/
        if ( [_personalBeacons indexOfObject:pbTemp] != NSNotFound){
             [_pairedBeaconLock  unlock];
            if (_soundOn) {
                [self performSelectorOnMainThread:@selector(makeSomeNoise) withObject:nil waitUntilDone:false];
            }
            if (_videoTriggeringOn){
                [self startVideoRecording];
            } else {
                //Check if we are recording.
                if (_currentlyRecording){
                    //Toggle to stop it.
                    [self stopVideoRecording];
                }
            }
//            [self makeSomeNoise];
        } else {
            //So, checks to see if we are recording.
            //Whether we are suppose to or not, if we hit this, then we should NOT
            //be recording...right?
            [self stopVideoRecording];
             [_pairedBeaconLock  unlock];
        }
//        long personalSize = [_personalBeacons count];
//        for (int j = 0 ; j < personalSize; j++){
//            PairingBeacon *mPB = (PairingBeacon *) _personalBeacons[j];
//            if ( [mPB.uuid isEqualToString:pbTemp.uuid] ){
//                [self makeSomeNoise];
//            }
//        }
        
    }
    
}

-(void)makeSomeNoise{
    /*Make some noise!*/
//    NSError* error;
//    [[AVAudioSession sharedInstance]
//     setCategory:AVAudioSessionCategoryPlayAndRecord
//     error:&error];
//    if (error == nil) {
//        SystemSoundID myAlertSound;
//        NSURL *url = [NSURL URLWithString:@"/System/Library/Audio/UISounds/new-mail.caf"];
//        AudioServicesCreateSystemSoundID((__bridge CFURLRef)(url), &myAlertSound);
//        
//        AudioServicesPlaySystemSound(myAlertSound);
//        NSLog(@"Wailing like a banshee!");
//    }

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error: nil];
    UInt32 route = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute, sizeof(route), &route);
    
    
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    NSLog(@"Wailing like a banshee!");
    
}

// This method will check the current state of recording.
// If it not currently recording it will start recording.
// If it is still recording, it will ignore and just return.
-(void)startVideoRecording{
    if (!_currentlyRecording){
        [_vts toggleRecording];
        _currentlyRecording = !_currentlyRecording;
    }
}

//Stops the video recording, only if it is currently recording though.
-(void)stopVideoRecording{
    if (_currentlyRecording){
        [_vts toggleRecording];
        _currentlyRecording = !_currentlyRecording;
    }
    //Make sure it is done recording before yeah.
    while(![_vts doneRecording]){}
}


- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    // This method fires when the receiver detects that it has entered the
    // defined beacon region. I disabled this so that we start ranging any
    // beacons even before we enter the region.
    //[self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    // This method fires when the device leaves the defined Beacon Region.
    //[self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    //self.statusLabel.text = @"Exited Region";
}

-(BOOL)isConcurrent {
    return YES;
}

-(BOOL)isExecuting {
    return executing;
}

-(BOOL)isFinished {
    return finished;
}

//If passed in true, the service will record a video
//for as long as it can hear a beacon from the subscribe to
//list.
//If passed in false, will stop this action if it is occuring.
-(void)allowVideoTriggering:(BOOL)flag{
    _videoTriggeringOn = flag;
    
}
//If passed in true, will make an audible noise when a beacon is close by.
//If passed in false, will stop making noise.
-(void)allowSound:(BOOL)flag{
    _soundOn = flag;
}

@end
