//
//  NetworkIO.m
//  
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "NetworkIO.h"

@interface NetworkIO()

@end

@implementation NetworkIO


+(void)sendFileToServerFolderName:(NSMutableString *)folder dataToSend:(NSMutableString *)data fileName:(NSMutableString *)fileName error:(NSError **)incError
{
    //loggeddata = [NSMutableString stringWithFormat:@"SmartLapSwimCapIOS"];
    NSData *dataToSend = [data dataUsingEncoding:NSUTF8StringEncoding];
    
    //NSURL *url = [NSURL URLWithString:@"http://50.63.158.199:8080/REST/UploadServlet"];
    //NSURL *url = [NSURL URLWithString:@"http://192.168.168.127:8080/REST/UploadServlet"];
    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://208.109.53.180:8080/ProObject_Servlet/FileUpload"]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"b034dc8x";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    //[body appendData:[[NSString stringWithFormat:@"Content-Type: multipart/form-data; boundary=%@", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param1\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", folder] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //ZTC
    //Added this to satisfy new length param
    NSString *length = [NSString stringWithFormat:@"%d", data.length];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param2\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", length] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Added this to satisfy new append param
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param3\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"APPEND"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //ZTC
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@.txt\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:dataToSend];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSHTTPURLResponse *response;
    NSError *error;
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            //success
        }
    }];
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:@"Failed to do something wicked" forKey:NSLocalizedDescriptionKey];
    *incError = [NSError errorWithDomain:@"myDomain" code:error.code userInfo:errorDetail];
    
}


+(NSString *) getFileFrom: (NSString *)filePath errorCode:(NSError**) errorCodePointer{
    
    //NSLog(@"TEST");
    NSData *fileNameData = [filePath dataUsingEncoding:NSUTF8StringEncoding];
    
    //get file contents using filepath from server
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://208.109.53.180:8080/ProObject_Servlet/FileRetrieve"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"POST"];
    //[request setValue:fileName forHTTPHeaderField:@"Cookie"];
    // NSInputStream * nis = [[NSInputStream alloc] initWithData:fileNameData];
    //[request setHTTPBodyStream:nis];
    [request setHTTPBody:fileNameData];
    
    NSError *error = nil;
    NSURLResponse *urlResponse = nil;
    
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    
    *errorCodePointer = error;
    
    
    NSString* responseString;
    responseString = [[NSString alloc] initWithData:response1 encoding:NSASCIIStringEncoding];
    //NSLog(@"log statement: %@", responseString);
    if ([responseString isEqualToString:@"File does not exist"]){
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:@"File does not exist" forKey:NSLocalizedDescriptionKey];
        *errorCodePointer = [NSError errorWithDomain:@"myDomain" code:error.code userInfo:errorDetail];
    } else {
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:@"Success" forKey:NSLocalizedDescriptionKey];
        *errorCodePointer = [NSError errorWithDomain:@"myDomain" code:error.code userInfo:errorDetail];
    }
    return responseString;
}
+(NSMutableArray *) listOfFileNames:(NSError**) errorCodePointer{
    NSLog(@"TEST!");
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://208.109.53.180:8080/ProObject_Servlet/FileList"]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:10];
    
    [request setHTTPMethod: @"GET"];
    
    //NSError *requestError;
    NSURLResponse *urlResponse = nil;
    NSError *error = nil;
    
    NSData *response1 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
    
    NSString* responseString;
    responseString = [[NSString alloc] initWithData:response1 encoding:NSASCIIStringEncoding];
    
    NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
    [errorDetail setValue:@"Failed to do something wicked" forKey:NSLocalizedDescriptionKey];
    *errorCodePointer = [NSError errorWithDomain:@"myDomain" code:error.code userInfo:errorDetail];
    
    
    NSLog(@"log statement: %@", error.localizedDescription);
    NSLog(@"log statement: %d", error.code);
//    if (*errorCodePointer == -1001){
//        return nil;
//    }
    //split the string by commas
    NSArray* delimitedStrings = [responseString componentsSeparatedByString: @","];
    NSMutableArray *legitFilePaths = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < delimitedStrings.count; i++)
    {
        if ([[delimitedStrings objectAtIndex:i] rangeOfString:@"mumboCombo"].location != NSNotFound)
        {
            if ([[delimitedStrings objectAtIndex:i] rangeOfString:@"badMC"].location == NSNotFound){
            [legitFilePaths addObject:[delimitedStrings objectAtIndex:i]];
            }
        }
    }

    
    return legitFilePaths;
}

@end
