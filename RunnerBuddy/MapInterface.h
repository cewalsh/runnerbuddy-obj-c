//
//  MapInterface.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapInterface : NSObject

//With this init, we can control what we need to on the UI (hopefully)
//  may need to pass in the Map Object if I can't yank it from the ParentView from here.
-(id)initWithParentView:(UIView *) parentView andMapView: (MKMapView*)incMap;
@property NSString *currentDateToWorkWith;
@property BOOL mapLoaded;
//Provide all trigger options, void no returns, mutate the map.
-(BOOL)newFollowing;
-(int)resetDate:(NSString *)newDate;
-(void)plotAllPoints;
-(void)plotLastPoint;
-(void)erasePoints;
-(void)findFriends;
-(void)toggleAutoRefresh;
-(BOOL)checkAutoRefresh;
-(NSMutableArray *)getDatesForPeepsChecked;
-(int)loadMap;
-(int)refresh;
-(NSMutableArray *)getCurrentPeeps;
-(NSArray *)getAllDates;

@end
