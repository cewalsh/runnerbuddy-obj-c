//
//  ChooseDateTableViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "ChooseDateTableViewController.h"
#import "BenjaminsDate.h"
#import <UIKit/UIKit.h>
#import "Toastview.h"

@interface ChooseDateTableViewController ()
@property NSArray *dates;
@property NSArray *specDates;
@property NSIndexPath *currentSelectedRowPath;
@property UITableView *myCrazyTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingInd;
@property int lastCheckedItem;
@end

@implementation ChooseDateTableViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.lastCheckedItem = -1;
    _loadingInd.color = UIColor.darkGrayColor;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSString *lastDate;
    if (!_useGoogleMaps){
        self.dates = [self.mapInterface getDatesForPeepsChecked];
        lastDate = _mapInterface.currentDateToWorkWith;
    } else {
        self.dates = [self.gMapInterface getDatesForPeepsChecked];
        lastDate = _gMapInterface.currentDateToWorkWith;
    }
    _specDates = [[NSMutableArray alloc] init];
    long numDates = [self.dates count];
    
    
    
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    for(int i =0; i < numDates; i++){
        BenjaminsDate *tempDate = [[BenjaminsDate alloc] init];
    
            tempDate.checked = false;
    
        
        tempDate.theDate = self.dates[i];
        tempDate.goodDate = true;
        [tempArray addObject:tempDate];
    }
    self.specDates = [tempArray sortedArrayUsingSelector:@selector(compare:)];
    long size = [_specDates count];
    for (int i = 0; i < size; i++){
        if ( [lastDate isEqualToString:((BenjaminsDate *)_specDates[i]).theDate]){
            ((BenjaminsDate *)_specDates[i]).checked = true;
            _lastCheckedItem = i;
            break;
        }
    }
    //[self toast:_dates[0]];
}
- (IBAction)doneButtonListener:(id)sender {
    
    [_loadingInd startAnimating];
    [self performSelector:@selector(becauseIHaveTo) withObject:nil afterDelay:0.01];
    
 
    
}

-(void)becauseIHaveTo{
    //Dealing with this guy later.
    if (self.source == 3){
        //return to the options menu
        //This guy tries to load the map, if he fails he tells us. Nice guy.
        if(_lastCheckedItem == -1){
            [self displayMessage:@"You need to pick a date before you can click done. Or, if you don't wish to proceed click Cancel." withTitle:@"Oops!"];
            return;
        }
        int returnCode;
        if (!_useGoogleMaps){
            returnCode = [self.mapInterface resetDate:((BenjaminsDate *)_specDates[self.lastCheckedItem]).theDate];
        } else {
            returnCode = [self.gMapInterface resetDate:((BenjaminsDate *)_specDates[self.lastCheckedItem]).theDate];
        }
        [_loadingInd stopAnimating];
        if (returnCode == 1){
            [self performSegueWithIdentifier: @"unwindFromDatesToOptions" sender: self];
        }
        
        
        
        //Now, this is when we are doing a new following, first need to check if we are kosher to go back
        //back to the main menu, if we aren't that means that the user picked a bad date.
        //We need to let them know, and probably do something to make sure they don't pick that
        //date again.
    } else {
        
        //return to the map.
        //prepare map.
        //If we get the go ahead, we will perform the segue.
        if(_lastCheckedItem == -1){
            [self displayMessage:@"You need to pick a date before you can click done. Or, if you don't wish to proceed click Cancel." withTitle:@"Oops!"];
            return;
        }
        if ([self shouldPerformSegueWithIdentifier:@"unwindToMapViewFromDates" sender:nil]) {
            [self performSegueWithIdentifier: @"unwindToMapViewFromDates" sender: self];
        }
        
        
        
    }
    
}

- (IBAction)cancelButtonListener:(id)sender {
    //Depending on source, may go back to the map.
    if (_source == 6){
        [self performSegueWithIdentifier:@"unwindToMapViewFromDates" sender:self];
    } else {
        [self performSegueWithIdentifier:@"unwindFromDatesToOptions" sender:self];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.specDates count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Store the tableView for later.
    if (_myCrazyTable == nil){
        _myCrazyTable = tableView;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListPrototypeCell2" forIndexPath:indexPath];
    BenjaminsDate *date = (BenjaminsDate *)self.specDates[indexPath.row];
    cell.textLabel.text = [self makeDatePretty:date.theDate];
    //[self toast:@"Setting the cells!"];
    // Configure the cell...
    if(!date.goodDate){
        //If this is the case, will change style to grey..indicating a no go, repeat a no go.
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.textLabel.backgroundColor = UIColor.redColor;
        cell.backgroundColor = UIColor.darkGrayColor;
    } else {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.textLabel.backgroundColor = UIColor.whiteColor;
        cell.backgroundColor = UIColor.whiteColor;
    }
    if(date.checked){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

- (NSString *)makeDatePretty:(NSString *)uglyDate{
    
    NSArray *splittingUglyDate = [uglyDate componentsSeparatedByString:@"_"];
    
    
    return [NSString stringWithFormat:@"%@/%@/%@", splittingUglyDate[1], splittingUglyDate[2], splittingUglyDate[0] ];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BenjaminsDate *tappedItem = [self.specDates objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (!tappedItem.goodDate){
        [self toast:@"The date you picked does not work. Please pick another date."];
        return;
    }
    if(self.lastCheckedItem == -1){
        self.lastCheckedItem = indexPath.row;
        _currentSelectedRowPath = indexPath;
        //tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        tappedItem.checked = true;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    } else if ( indexPath.row != self.lastCheckedItem ){
    
        //[tableView deselectRowAtIndexPath:indexPath animated:NO];
        tappedItem.checked = true;
        //must remove the other date checked, look through list for other checked.
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        NSIndexPath *original = indexPath;
        int newRow = self.lastCheckedItem;
        ((BenjaminsDate*)self.specDates[_lastCheckedItem]).checked = false;
        NSIndexPath *changedRow = [NSIndexPath
                                   indexPathForRow:newRow              // Use the new row
                                   inSection:original.section];  // Use the original section
        [tableView reloadRowsAtIndexPaths:@[changedRow] withRowAnimation:UITableViewRowAnimationTop];
        self.lastCheckedItem = indexPath.row;
        _currentSelectedRowPath = indexPath;
    }
    
}


//Method catches segues and makes sure they are good to go (as far as checking whether or not criteria is met)
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"unwindToMapViewFromDates"]){
        
        
        if (!_useGoogleMaps){
            self.mapInterface.currentDateToWorkWith = ((BenjaminsDate *)_specDates[self.lastCheckedItem]).theDate;
        } else {
            self.gMapInterface.currentDateToWorkWith = ((BenjaminsDate *)_specDates[self.lastCheckedItem]).theDate;
        }
        //This guy tries to load the map, if he fails he tells us. Nice guy.
        
        int returnCode;
        
        if (!_useGoogleMaps){
            returnCode = [self.mapInterface loadMap];
        } else {
            returnCode = [self.gMapInterface loadMap];
        }
        [_loadingInd stopAnimating];
        switch (returnCode) {
            case 1:{
                
                return true;
            }
            case 2:{
                [self displayMessage:@"Connectivity issues, check your WiFi/4g" withTitle:@"Oops!"];
                break;
            }
            case 3:{
                [self displayMessage:@"The date you picked doesn't work. pPlease choose another." withTitle:@"Oops!"];
                BenjaminsDate *date = _specDates[_lastCheckedItem];
                date.goodDate = false;
                date.checked = false;
                //[_myCrazyTable reloadRowsAtIndexPaths:@[_currentSelectedRowPath] withRowAnimation:UITableViewRowAnimationTop];
                //Null out our current row info!
                _lastCheckedItem = -1;
                _currentSelectedRowPath = nil;
                break;
            }
            default:{
                break;
            }
        }
        return false;
    } else {
        return true;
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)displayMessage: (NSString *)message withTitle:(NSString*)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];

    [self presentViewController:alert animated:YES completion:nil];
}

-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.view withText:message withDuaration:3.0];
}

@end
