//
//  OptionsViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "OptionsViewController.h"
#import "ChoosePersonTableViewController.h"
#import "ChooseDateTableViewController.h"
#import "ToastView.h"

@interface OptionsViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *autoRefreshToggle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingInd;

@end

@implementation OptionsViewController
- (IBAction)unwindToOptions:(UIStoryboardSegue *)segue{
    //Do nothing
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Set the switch we created with stuff?
    //First, set the value of the autoRefreshToggle.
    if (!_useGoogleMaps){
        [self.autoRefreshToggle setOn:[self.mapInterface checkAutoRefresh] ];
    } else {
        [self.autoRefreshToggle setOn:[self.gMapInterface checkAutoRefresh] ];
    }
    
    [self.autoRefreshToggle addTarget:self
                              action:@selector(autoRefreshWasToggled)
                    forControlEvents:UIControlEventValueChanged];
}

-(void)autoRefreshWasToggled{
//    [self toast: [self.mapInterface checkAutoRefresh] ? @"ON" : @"OFF" ];
    if (!_useGoogleMaps){
        [self.mapInterface toggleAutoRefresh];
    } else {
        [self.gMapInterface toggleAutoRefresh];
    }
//   [self toast: [self.mapInterface checkAutoRefresh] ? @"ON" : @"OFF" ];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.view withText:message withDuaration:3.0];
}

//My fancy prepareForSegue...that parades each day! at sunset and vine..
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"fromNewFollowing"]){
       
        
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ChoosePersonTableViewController *controller = (ChoosePersonTableViewController *)navController.topViewController;
        controller.source = 1;
        if (!_useGoogleMaps) {
            controller.mapInterface = self.mapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.gMapInterface;
            controller.useGoogleMaps = true;
        }
    } else if ([segue.identifier isEqualToString:@"fromNewPersons"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ChoosePersonTableViewController *controller = (ChoosePersonTableViewController *)navController.topViewController;
        controller.source = 2;
        if (!_useGoogleMaps) {
            controller.mapInterface = self.mapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.gMapInterface;
            controller.useGoogleMaps = true;
        }
    } else if ([segue.identifier isEqualToString:@"fromOptionsToDate"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        ChooseDateTableViewController *controller = (ChooseDateTableViewController *)navController.topViewController;
        controller.source = 3;
        
        if (!_useGoogleMaps) {
            controller.mapInterface = self.mapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.gMapInterface;
            controller.useGoogleMaps = true;
        }
    } else if ([segue.identifier isEqualToString:@"goingHome"]){
        //Check if map is loaded.
        if (!_useGoogleMaps) {
            if (_mapInterface.mapLoaded){
                //call refresh with new data.
                [_loadingInd startAnimating];
                [_mapInterface performSelector:@selector(refresh) withObject:nil afterDelay:0.01];
                [_loadingInd stopAnimating];
            }
        } else {
            if (_gMapInterface.mapLoaded){
                //call refresh with new data.
                [_loadingInd startAnimating];
                [_gMapInterface performSelector:@selector(refresh) withObject:nil afterDelay:0.01];
                [_loadingInd stopAnimating];
            }
        }
        
        
    }
}

- (IBAction)newFollowButtonListener:(id)sender {
    
    [_loadingInd startAnimating];
    
    // mapToNewFollowing
    [self performSelector:@selector(prepMapInterface) withObject:nil afterDelay:0.01];
    
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if([identifier isEqualToString:@"fromNewFollowing"]){
        return true;
    } else if ([identifier isEqualToString:@"fromNewPersons"]){
        if (!_useGoogleMaps) {
            if (_mapInterface.mapLoaded){
                return true;
            } else {
                [self displayMessage:@"You need to have loaded up a\nnew following before you can change the people." withTitle:@"Oops!"];
                return false;
            }
        } else {
            if (_gMapInterface.mapLoaded){
                return true;
            } else {
                [self displayMessage:@"You need to have loaded up a\nnew following before you can change the people." withTitle:@"Oops!"];
                return false;
            }
        }
        
    } else if ([identifier isEqualToString:@"fromOptionsToDate"]) {
        if (!_useGoogleMaps) {
            if (_mapInterface.mapLoaded){
                return true;
            } else {
                [self displayMessage:@"You need to have loaded up a new following before you can change the people." withTitle:@"Oops!"];
                return false;
            }
        } else {
            if (_gMapInterface.mapLoaded){
                return true;
            } else {
                [self displayMessage:@"You need to have loaded up a new following before you can change the people." withTitle:@"Oops!"];
                return false;
            }
        }
    } else if ([identifier isEqualToString:@"goingHome"]){

        return true;
    }
    return true;
}

-(void)prepMapInterface{
    int numOfPeople;
    int count = 0;
    if (!_useGoogleMaps) {
        [self.mapInterface newFollowing];
        do{
        
        
            numOfPeople = [[self.mapInterface getCurrentPeeps] count];
            if(numOfPeople == 0){
                [self.mapInterface newFollowing];
            }
            count++;
        }while (numOfPeople == 0 && count <3);
    } else {
        [self.gMapInterface newFollowing];
        do{
            
            
            numOfPeople = [[self.gMapInterface getCurrentPeeps] count];
            if(numOfPeople == 0){
                [self.gMapInterface newFollowing];
            }
            count++;
        }while (numOfPeople == 0 && count <3);
    }
    [_loadingInd stopAnimating];
    if(numOfPeople != 0){
        [self performSegueWithIdentifier:@"fromNewFollowing" sender:self];
    } else {
        [self displayMessage:@"Unable to access the internet, please\nmake sure you are connected and try again." withTitle:@"Oops!"];
    }
}

-(void)displayMessage: (NSString *)message withTitle:(NSString*)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
