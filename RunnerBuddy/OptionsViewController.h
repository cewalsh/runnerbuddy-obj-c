//
//  OptionsViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapInterface.h"
#import "GoogleMapsInterface.h"
@interface OptionsViewController : UIViewController
- (IBAction)unwindToOptions:(UIStoryboardSegue *)segue;
@property BOOL useGoogleMaps;
@property (nonatomic) MapInterface *mapInterface;
@property (nonatomic) GoogleMapsInterface *gMapInterface;
@end
