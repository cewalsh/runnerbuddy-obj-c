//
//  Person.h
//      ~Represents a Person, which includes a name and a collection of Runs associated with the Person.
//      ~This information is obtained via a list of file names (that contain the same username).
//
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property NSString *name;
-(id) initWithListOfFiles: (NSMutableArray *) listOfFiles andName:(NSString *) userName;
@property BOOL following;
//-(NSMutableArray *) getPointsFromDate:(NSString *) date;
-(NSMutableSet*)getSetOfDates;
-(NSMutableArray*)getMostRecentFromDate:(NSString *)date withError:(NSError**)error;
-(NSComparisonResult)compare:(Person *)otherObject;

@end
