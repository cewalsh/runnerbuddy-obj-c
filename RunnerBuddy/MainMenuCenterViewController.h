//
//  MainMenuCenterViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
@interface MainMenuCenterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *runnerModeButton;
@property (weak, nonatomic) IBOutlet UIButton *spectatorModeButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;
@property MainViewController *myOldMan;
@end
