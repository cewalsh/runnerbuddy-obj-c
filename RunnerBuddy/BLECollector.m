//
//  BLECollector.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/22/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "BLECollector.h"
#import "BenjaminsBeacon.h"
#import "SuperService.h"

@interface BLECollector ()
@property CLLocation  *currentLoco;
@property NSMutableArray *currentBeacons;
@property NSLock *currentBeaconsLock;
@property SharedData *sd;
#define sleepyTime [NSThread sleepForTimeInterval:0.125];
@end


@implementation BLECollector

-(id)initWithSharedData:(SharedData *)sd{
    self = [super init];
    if (self) {
        executing = NO;
        finished = NO;
    }
    _sd = sd;
    _currentBeaconsLock = [[NSLock alloc] init];
    _currentBeacons = [[NSMutableArray alloc] init];
    // Initialize location manager and set ourselves as the delegate

    
    return self;
}

- (void)start {
    // Always check for cancellation before launching the task.
    if ([self isCancelled])
    {
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }

    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}


-(void)forMainThread{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    [_locationManager requestAlwaysAuthorization];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    _myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.proobject.testregion"];
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
    
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

- (void)main {
    @try {
        
        if ( [self isCancelled]) {
            [self completeOperation];
            return;
        }
        [self performSelectorOnMainThread:@selector(forMainThread) withObject:nil waitUntilDone:false];
        int count;
        // Do the main work of the operation here.
        while (![self isCancelled]) {
            /*Every tens seconds, I'll try and add the stuff to the data bucket.*/
            count = 1;
            while (![self isCancelled] && count != 24){
                sleepyTime
                count++;
            }
            /*Now try to access data for data bucket.*/
            NSLog(@"My boy baecon collector's main is strying to get the current beacons lock.");
            [_currentBeaconsLock lock];
            [_sd.dataBucketLock lock];
            NSLog(@"BLE collector has the data bucket lock.");
            /*Two parts too transfer, add objects and remove current beacons.*/
            [_sd.dataBucket addObjectsFromArray:_currentBeacons];
            [_currentBeacons removeAllObjects];
            
            [_sd.dataBucketLock unlock];
            NSLog(@"BLE collector has unlocked the data bucket lock.");
            [_currentBeaconsLock unlock];
            NSLog(@"BLE collector has unlocked the current beacon lock");
        }
        
        
        [self completeOperation];
    }
    @catch(...) {
        // Do not rethrow exceptions.
    }
}

- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    // This method fires when the receiver detects that it has entered the
    // defined beacon region. I disabled this so that we start ranging any
    // beacons even before we enter the region.
    //[self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    // This method fires when the device leaves the defined Beacon Region.
    //[self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    //self.statusLabel.text = @"Exited Region";
}

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    int test;
    if (self == nil) {
        test = 0;
        NSLog(@"Self is null");
        return;
    }
    
    if ( [self isCancelled]) {return;}
    NSLocale* currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy_MM_dd:HH_mm_ss"];
    NSString *currentTime = [DateFormatter stringFromDate:[NSDate date]];
    NSLog(@"My boy the location listen is trying to get the current beacon lock.");
    // At least 1 beacon found!
    [_currentBeaconsLock lock];
    NSLog(@"BLE collector has current beacon lock");
    // Determine number of beacons found and update the View.
    NSUInteger numBeacons = [beacons count];
    if (numBeacons == 0){
        //probably should unlock.
        [_currentBeaconsLock unlock];
        return;
    }
    
    float latitude;
    float longitude;
    /*Need to acquire the locoLock so I can get the current location.*/
    [_sd.currentLocoLock lock];
    NSLog(@"BLE collector has loco lock");
    if (_sd.pleaseWork == nil){
        [_sd.currentLocoLock unlock];
        return;
    }
    latitude = _sd.pleaseWork.coordinate.latitude;
    longitude = _sd.pleaseWork.coordinate.longitude;
    
    [_sd.currentLocoLock unlock];
    NSLog(@"BLE collector has unlocked loco lock");
    /*Got the lat and long. Now to tag the beacons with it?*/
    
    
    /*Here's my quick alg. Not even tho.*/
    /*If (for a particular beacon) the RSSI is stronger, we'll assume they are 
     'closer' and use the current loco, otherwise stick with old.*/
    for (int i = 0; i < numBeacons; i++){
        CLBeacon *temp = (CLBeacon *)beacons[i];
        NSString *majorValue = [NSString stringWithFormat:@"%4lX",
                                (unsigned long)[temp.major integerValue]];
        NSString *minorValue =[NSString stringWithFormat:@"%4lX",
                               (unsigned long)[temp.minor integerValue]];
      
        
        minorValue = [minorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        majorValue = [majorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        
        NSString *tempName = [NSString stringWithFormat:@"%@%@%@", [temp.proximityUUID.UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""], majorValue, minorValue];
        tempName = [tempName lowercaseString];
//        NSLog(@"UUID: %@", tempName);
//        NSLog(@"Major: %@", majorValue);
//        NSLog(@"Minor: %@", minorValue);
        //First, check to see if we already have this bloke.
        BOOL notFound = true;
        long size = [_currentBeacons count];
        for (int j = 0; j < size; j++){
            BenjaminsBeacon *tempB = (BenjaminsBeacon *)_currentBeacons[j];
            if ( [tempName isEqualToString:tempB.uuid] ){
                //Have beacon, check RSSI.
                notFound = false;
                if (temp.rssi > tempB.rssi){
                    /*If rssi is greater...i.e. stronger, than change coord.*/
                    tempB.rssi = temp.rssi;
                    tempB.latitude = latitude;
                    tempB.longitude = longitude;
                    tempB.timeCollected = currentTime;
                }
                /*Regardless, we found the beacon, so skip out.*/
                break;
            }
        }
        if(notFound) {
            /*Need to add to list.*/
            BenjaminsBeacon *toAdd = [[BenjaminsBeacon alloc] init];
            toAdd.uuid = tempName;
            toAdd.rssi = temp.rssi;
            toAdd.latitude = latitude;
            toAdd.longitude = longitude;
            toAdd.timeCollected = currentTime;
            [_currentBeacons addObject:toAdd];
        }
    }
    /*Nothing else left to do.*/
    [_currentBeaconsLock unlock];
    NSLog(@"BLE collector has unlocked the current beacon lock");
}


-(void)stopLocation{
    [self.locationManager stopMonitoringForRegion:self.myBeaconRegion];
    
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    
    _locationManager.delegate = nil;
}

- (void)completeOperation {
    [self performSelectorOnMainThread:@selector(stopLocation) withObject:nil waitUntilDone:false];

    /*Gotta pause, and give the location listener a second or so to get rid of any left over guys.*/
    sleepyTime
    
    [_currentBeaconsLock lock];
    [_sd.currentLocoLock lock];
    [_sd.currentLocoLock unlock];
    [_currentBeaconsLock unlock];
    
    
    
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
    
    NSLog(@"Completed BLECollector");
}

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished {
    return finished;
}



@end
