//
//  BenjaminsPoint.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "BenjaminsPoint.h"

@interface BenjaminsPoint()



@end

@implementation BenjaminsPoint
@synthesize coordinate, title, subtitle;
//Implementing my own point, so I can write a special compare for sorting.
-(NSComparisonResult)compare:(BenjaminsPoint *)otherObject{
    
    
    //if a is bigger than b, descending return,
    if (self.time < otherObject.time){
        return NSOrderedDescending;
    //if b is bigger than a, ascending return,
    } else if (self.time > otherObject.time){
        return NSOrderedAscending;
    //if equal, equal return
    } else {
        return NSOrderedSame;
    }
    
}


@end
