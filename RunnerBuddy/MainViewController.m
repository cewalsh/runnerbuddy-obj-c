//
//  MainViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "MainViewController.h"
#import "MainMenuCenterViewController.h"
#import "MainMenuLeftViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PairWithBeaconTableViewController.h"
#import "PairingBeacon.h"
#import "FileIO.h"
#define BeaconFileName @"beaconMemory"
#define CENTER_TAG 1
#define LEFT_PANEL_TAG 2
#define CORNER_RADIUS 4
#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface MainViewController () <UIGestureRecognizerDelegate>
@property (nonatomic, strong) MainMenuCenterViewController *centerViewController;
@property (nonatomic, strong) MainMenuLeftViewController *leftPanelViewController;
@property (nonatomic, assign) BOOL showingLeftPanel;
@property (nonatomic, assign) BOOL showPanel;
@property (nonatomic, assign) CGPoint preVelocity;
@end

@implementation MainViewController
- (IBAction)unwindToMainMenu:(UIStoryboardSegue *)segue{}
- (void)viewDidLoad {
    [super viewDidLoad];
    /*Insert code for allowing ambiet noise
     {  // Set audio category to ambient - allows music to coexist with the application
     NSError *error;
     _audioSession = [AVAudioSession sharedInstance];
     [session setCategory: AVAudioSessionCategoryAmbient error: &error];
     if (error) {
     NSLog(@"setCategory failed %@",error);
     }
     }
     */
    
    
    // Do any additional setup after loading the view.
    _showingLeftPanel = false;
    _listeningToBLE = false;
    _videoTriggering = false;
    _pairedBeacons = [[NSMutableArray alloc] init];
    _pairedBeaconsLock = [[NSLock alloc] init];
    _opsQueue = [[NSOperationQueue alloc] init];
    [self setupView];
    [self getPairedBeacons];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    id toggleSwitchValue= [standardUserDefaults objectForKey:@"rrhAlwaysOn"];
    _listeningForRunners = [toggleSwitchValue boolValue];
    //Check if listenForRunners is on. it is, we need to create the
    //super service.
    if (_listeningForRunners){
        if (nil == _superService){
            /*Turn on the Listen For Runners Activity.*/
            _superService = [[SuperService alloc] init];
            @try {
                [_opsQueue addOperation:_superService];
            } @catch (NSException *e){
                NSLog(@"oh noes");
            }

        }
    }
}

- (UIView *)getLeftView
{
    // init view if it doesn't already exist
    if (_leftPanelViewController == nil)
    {
        // this is where you define the view for the left panel
        self.leftPanelViewController = [[MainMenuLeftViewController alloc] initWithNibName:@"MainMenuLeft" bundle:nil];
        self.leftPanelViewController.view.tag = LEFT_PANEL_TAG;
        _leftPanelViewController.myPop = self;
        [_leftPanelViewController setToggles];
        [self.view addSubview:self.leftPanelViewController.view];
        
        [self addChildViewController:_leftPanelViewController];
        [_leftPanelViewController didMoveToParentViewController:self];
        
        _leftPanelViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    
    self.showingLeftPanel = YES;
    
    // set up view shadows
    [self showCenterViewWithShadow:YES withOffset:-2];
    
    UIView *view = self.leftPanelViewController.view;
    return view;
}

- (void)showCenterViewWithShadow:(BOOL)value withOffset:(double)offset{
    if (value)
    {
        [_centerViewController.view.layer setCornerRadius:CORNER_RADIUS];
        [_centerViewController.view.layer setShadowColor:[UIColor blackColor].CGColor];
        [_centerViewController.view.layer setShadowOpacity:0.8];
        [_centerViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
        
    }
    else
    {
        [_centerViewController.view.layer setCornerRadius:0.0f];
        [_centerViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
}

-(void)setupView{
    _centerViewController = [[MainMenuCenterViewController alloc] initWithNibName:@"MainMenuCenter" bundle:nil];
    
    [self.view addSubview:_centerViewController.view];
    [self addChildViewController:_centerViewController];
    _centerViewController.view.frame = self.view.frame;
    _centerViewController.view.userInteractionEnabled = true;
    _centerViewController.myOldMan = self;
    self.centerViewController.view.tag = CENTER_TAG;
    [_centerViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:_centerViewController.view];
    [self setupGestures];
    
}

-(void)getPairedBeacons{
    NSString *fileContents = [FileIO getDataFromSavedFile:BeaconFileName];
    /*First, check to make sure our contents are legit...yo*/
    if (fileContents == nil || [fileContents isEqualToString:@""]){
        return;
    }
    
    /*Valid, so power through them!*/
    NSArray *splitContents = [fileContents componentsSeparatedByString:@"\n"];
    long size = [splitContents count];
    for (int i = 0; i < size; i++){
        NSArray *line = [splitContents[i] componentsSeparatedByString:@","];
        if ([line count] < 8){ continue; }
        PairingBeacon *temp = [[PairingBeacon alloc] init];
        temp.uuid = line[0];
        temp.friendlyName = line[1];
        temp.majorValue = line[2];
        temp.minorValue = line[3];
        temp.rssi = [line[4] integerValue];
        temp.timeLastHeard = [line[5] doubleValue];
        temp.heardRecently = ( [line[6] integerValue] == 0 ) ? false : true;
        temp.pairedWith = ( [line[7] integerValue] == 0 ) ? false : true;
        if (temp.pairedWith){
            [_pairedBeaconsLock lock];
            if ( [_pairedBeacons indexOfObject:temp] == NSNotFound){
                [_pairedBeacons addObject:temp];
            }
            [_pairedBeaconsLock unlock];
        }
    }
}


- (void)setupGestures{
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    
    [_centerViewController.view addGestureRecognizer:panRecognizer];
}

-(void)movePanel:(id)sender{
    [[[(UITapGestureRecognizer*)sender view] layer] removeAllAnimations];
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
    CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        UIView *childView = nil;
        
        if(velocity.x > 0) {
            if (!_showingLeftPanel) {
                childView = [self getLeftView];
            }
        }
        // Make sure the view you're working with is front and center.
        [self.view sendSubviewToBack:childView];
        [[sender view] bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
        }
        
        if (!_showPanel) {
            [self hidingTheMenu];
        } else {
            if (_showingLeftPanel) {
                [self movingOutTheMenu];
            } else {
                [self hidingTheMenu];
            }
        }
    }
    
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {
        if(velocity.x > 0) {
            // NSLog(@"gesture went right");
        } else {
            // NSLog(@"gesture went left");
            
            if(!_showingLeftPanel){
                /*If we aren't showing the left panel, don't allow scrolling to right.*/
                return;
            }
            /*If we are, need to allow to navigate back home.*/
            
        }
        
        // Are you more than halfway? If so, show the panel when done dragging by setting this value to YES (1).
        _showPanel = abs([sender view].center.x - _centerViewController.view.frame.size.width/2) > _centerViewController.view.frame.size.width/2;
        
        // Allow dragging only in x-coordinates by only updating the x-coordinate with translation position.
        [sender view].center = CGPointMake([sender view].center.x + translatedPoint.x, [sender view].center.y);
        [(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0,0) inView:self.view];
        
        // If you needed to check for a change in direction, you could use this code to do so.
        if(velocity.x*_preVelocity.x + velocity.y*_preVelocity.y > 0) {
            // NSLog(@"same direction");
        } else {
            // NSLog(@"opposite direction");
        }
        
        _preVelocity = velocity;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)optionsButtonListener:(id)sender {
    /*Reveal the big stuff*/
    if (!_showingLeftPanel) {
        [self movingOutTheMenu];
    } else {
        [self hidingTheMenu];
    }
}

-(void)movingOutTheMenu{
    
    _centerViewController.runnerModeButton.userInteractionEnabled = false;
    _centerViewController.settingsButton.userInteractionEnabled = false;
    UIView *childView = [self getLeftView];
    [self.view sendSubviewToBack:childView];
    
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _centerViewController.view.frame = CGRectMake(self.view.frame.size.width - PANEL_WIDTH, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
//                         if (finished) {
//                             
//                             _centerViewController.leftButton.tag = 0;
//                         }
                     }];
}

-(void)hidingTheMenu{
    
    _centerViewController.runnerModeButton.userInteractionEnabled = true;
    _centerViewController.settingsButton.userInteractionEnabled = true;
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _centerViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             
                             [self resetMainView];
                         }
                     }];
}

- (void)resetMainView{
    /*Reset the main menu*/
    // remove left view and reset variables, if needed
    if (_leftPanelViewController != nil)
    {
        [self.leftPanelViewController.view removeFromSuperview];
        self.leftPanelViewController = nil;
        
        self.showingLeftPanel = NO;
    }
    
    // remove view shadows
    [self showCenterViewWithShadow:NO withOffset:0];
}


//My fancy prepareForSegue...that parades each day! at sunset and vine..
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"mainToPairBeacons"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        PairWithBeaconTableViewController *controller = (PairWithBeaconTableViewController *)navController.topViewController;
        controller.pairedBeacons = _pairedBeacons;
        controller.pairedBeaconsLock = _pairedBeaconsLock;
        
    }
}



@end
