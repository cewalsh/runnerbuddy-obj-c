//
//  RunnerModeInterface.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "RunnerModeInterface.h"
#import "ToastView.h"

/*Define some crazy variables in here!*/
@interface RunnerModeInterface()
//Variable if runnerMode is on or not, prevents miscalls to the class.
@property BOOL runnerModeOn;
@property BOOL constructorCalled;
@property UIView *parentView;
@property NSString *userName;
@property NSMutableString *fileName;
@property CLLocationManager* locationManager;
@property NSDate *lastUpdate;
@property NSInteger updateInterval;
@property NSMutableString *previousData;
@property UIBackgroundTaskIdentifier* bgTask;
@property NSTimer *timer;
@end


@implementation RunnerModeInterface 
-(void)RMI_Constructor:(UIView *) parentView{
    self.parentView = parentView;
    self.constructorCalled = true;
    self.runnerModeOn = false;
    _previousData = [NSMutableString stringWithFormat:@""];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestAlwaysAuthorization];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *updateInt = [standardUserDefaults objectForKey:@"prefUpdateInterval"];
    if (!updateInt || updateInt == 0)
    {
        self.updateInterval = 60;
    }
    else
    {
        self.updateInterval = [updateInt intValue];
    }
    
    NSString *user = [standardUserDefaults objectForKey:@"prefUserName"];
    if (!user || [user length] == 0)
    {
        self.userName = @"DefaultUser";
    }
    else
    {
        self.userName = user;
    }
    
    
}




-(void)startRunnerMode{
    if (self.constructorCalled != true || self.runnerModeOn != false){
        return;
    }
    
    self.runnerModeOn = true;
    //Begin Runner Mode.
    [self startRunning];
}
-(void)stopRunnerMode{
    if(self.constructorCalled != true || self.runnerModeOn != true){
        return;
    }
    self.runnerModeOn = false;
    [self.locationManager stopUpdatingLocation];
    //Turn runner mode off.
 
}




//RUNNER MODE
- (void)startRunning {

        NSDate *currentTime = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy_MM_dd:hh_mm_ss"];
        NSString *resultString = [dateFormatter stringFromDate: currentTime];
        self.fileName = [[NSMutableString alloc] initWithString:self.userName];
        [self.fileName appendString:@":"];
        [self.fileName appendString:resultString];
        
        [self.locationManager startUpdatingLocation];


}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //if time elapsed < interval, return
    NSDate *currentTime = [NSDate date];
    
    if (self.lastUpdate == Nil)
    {
        self.lastUpdate = currentTime;
    }
    else if (([currentTime timeIntervalSince1970] - [self.lastUpdate timeIntervalSince1970]) < self.updateInterval)
    {
        return;
    }
    
    self.lastUpdate = currentTime;
    
    CLLocation *loc = [locations lastObject];
    
    // Lat/Lon
    float latitude = loc.coordinate.latitude;
    float longitude = loc.coordinate.longitude;
    
    NSString *latString = [NSString stringWithFormat:@"%f", latitude];
    NSString *longString = [NSString stringWithFormat:@"%f", longitude];
    
    
    //:2014_12_23 12_35_37,39.16141282,-76.71107354,CaptainAmerica:
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy_MM_dd hh_mm_ss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSMutableString *data = [[NSMutableString alloc] initWithString:_previousData];
    [data appendString:resultString];
    [data appendString:@","];
    [data appendString:latString];
    [data appendString:@","];
    [data appendString:longString];
    [data appendString:@","];
    [data appendString:self.userName];
    [data appendString:@":\n"];
    
    
    
    [self sendFileToServer:[NSMutableString stringWithFormat:@"runnerInfo"] :data];
    
    //send to server, store locally as some devices only have wifi (our test device)
}


-(void)sendFileToServer:(NSMutableString *)folder :(NSMutableString *)data
{
    //loggeddata = [NSMutableString stringWithFormat:@"SmartLapSwimCapIOS"];
    NSData *dataToSend = [data dataUsingEncoding:NSUTF8StringEncoding];
    
    //NSURL *url = [NSURL URLWithString:@"http://50.63.158.199:8080/REST/UploadServlet"];
    //NSURL *url = [NSURL URLWithString:@"http://192.168.168.127:8080/REST/UploadServlet"];
    //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://208.109.53.180:8080/ProObject_Servlet/FileUpload"]];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"b034dc8x";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    //[body appendData:[[NSString stringWithFormat:@"Content-Type: multipart/form-data; boundary=%@", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param1\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", folder] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //ZTC
    //Added this to satisfy new length param
    NSString *length = [NSString stringWithFormat:@"%d", data.length];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param2\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", length] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Added this to satisfy new append param
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //[body appendData:[@"Content-Type: text/plain\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param3\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"APPEND"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    //ZTC
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@.txt\"\r\n", self.fileName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:dataToSend];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSHTTPURLResponse *response;
    NSError *error;
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        if(data.length > 0)
//        {
//            //success
//            [self toast:@"Yay, I sent up!"];
//        } else {
//            [self toast:@"Error error Wil Robinson!"];
//            long myCode = [(NSHTTPURLResponse *)response statusCode];
//            [self toast:(@"error: %@", ([NSString stringWithFormat:@"%ld",myCode]))];
//        }
    }];
    NSLog(@"%@",response.description);
    /*Zero means no error occured.*/
    if (error.code != 0){
        [_previousData appendString:data];
    } else {
        _previousData = [NSMutableString stringWithFormat:@""];
    }
}


-(void)applicationDidEnterBackground {
    [self.locationManager stopUpdatingLocation];
    
    UIApplication*    app = [UIApplication sharedApplication];
    
    self.bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:self.bgTask];
        self.bgTask = UIBackgroundTaskInvalid;
    }];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.updateInterval
                                                  target:self.locationManager
                                                selector:@selector(startUpdatingLocation)
                                                userInfo:nil
                                                 repeats:YES];
    
}


-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.parentView withText:message withDuaration:3.0];
}
@end
