//
//  SuperService.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/22/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "SuperService.h"
#import "GPSCollector.h"
#import "BLECollector.h"
#import "BenjaminsBeacon.h"
#import "NetworkIO.h"
#import "SharedData.h"

@interface SuperService ()
@property NSOperationQueue* opsQueue;
@property SharedData *sd;
@property CLLocation *currentLoco;
@property GPSCollector *gpsCollector;
@property BLECollector *bleCollector;
@property NSMutableArray *privateBucket;
#define sleepyTime [NSThread sleepForTimeInterval:0.125];
@end


@implementation SuperService

-(id)init {
    self = [super init];
    if (self) {
        executing = NO;
        finished = NO;
    }
    _sd = [[SharedData alloc] init];
    _sd.dataBucket = [[NSMutableArray alloc] init];
    _privateBucket = [[NSMutableArray alloc] init];
    return self;
}

-(BOOL)isConcurrent {
    return YES;
}

-(BOOL)isExecuting {
    return executing;
}

-(BOOL)isFinished {
    return finished;
}

-(void)start {
    //Gotta check to make sure we havn't been cancelled before we start.
    if ( [self isCancelled]) {
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    /*Alloc and init my locks.*/
    _sd.currentLocoLock = [[NSLock alloc] init];
    _sd.dataBucketLock  = [[NSLock alloc] init];
    _gpsCollector    = [[GPSCollector alloc] initWithSharedData:_sd];
    
    //If it wasn't cancelled, we good in the hood.
    _opsQueue = [[NSOperationQueue alloc] init];
    _bleCollector = [[BLECollector alloc] initWithSharedData:_sd];
    [_opsQueue addOperation:_gpsCollector];
    [_opsQueue addOperation:_bleCollector];
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)main {
    
    @try {
        
        /*Do Work.*/
        
        BOOL isDone = NO;
        int count;
        
        while (![self isCancelled] && !isDone) {
            /*Wait for 10 seconds..sleep? sure*/
            count = 1;
            while (![self isCancelled] && count != 80){
                sleepyTime
                count++;
            }
            /*Acquire the data bucket lock*/
            [_sd.dataBucketLock lock];
            NSLog(@"Super service has data bucket lock.");
            /*Remove data from data bucket to private structure.*/
            [_privateBucket addObjectsFromArray:_sd.dataBucket];
            [_sd.dataBucket removeAllObjects];
            /*Release the data bucket lock*/
            [_sd.dataBucketLock unlock];
            NSLog(@"Supser service has released the data bucket lock.");
            /*Attempt to push data from private structure*/
            /*If successful, clear private structure*/
            /*If failure, just wait till next 10 seconds.*/
            [self sendToServerPrivateBucket];
        }
        
        [self completeOperation];
    }
    @catch(NSException *exception){
        //Catching anything that's pitched. Phrasing?
    }
    
    
}


-(void)sendToServerPrivateBucket{
    /*Sending the private Bucket to server, if success, we'll celebrate with fat steer.
     If we fail. Well, guess it's more mushroom soup with goat milk tonight.*/
    
    /*Now, if the private bucket is empty, no reason to send up!*/
    if ([_privateBucket count] == 0) {return;}
    
    /*First, create the fileName*/
    
    NSLocale* currentLocale = [NSLocale currentLocale];
    [[NSDate date] descriptionWithLocale:currentLocale];
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy_MM_dd_HH_mm_ss"];
    NSMutableString *fileName = [NSMutableString stringWithFormat:@"RANDOMEVENT:%@:junk", [DateFormatter stringFromDate:[NSDate date]]];
    /*Now, gotta make our lovely data string.*/
    NSMutableString *data = [[NSMutableString alloc] initWithString:@""];
    int size = [_privateBucket count];
    for (int i = 0; i < size; i++){
        BenjaminsBeacon *temp = _privateBucket[i];
        [data appendString:temp.uuid];
        [data appendString:@", "];
        [data appendFormat:@"%f",temp.latitude];
        [data appendString:@", "];
        [data appendFormat:@"%f",temp.longitude];
        /*speed,bearing,alt,acc,ERT,time,*/
        [data appendString:@", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, "];
        [data appendString:temp.timeCollected];
        [data appendString:@",\n"];
        
    }
    NSError *error;
    /*Data string built...now to attempt a send off worthy of a viking king.*/
    [NetworkIO sendFileToServerFolderName:[NSMutableString stringWithFormat:@"ble_live"] dataToSend:data fileName:fileName error:&error];
    if (error.code == 200){
        /*Successful send up, clear private cache.*/
        [_privateBucket removeAllObjects];
    }
}

-(void)completeOperation{
    [_gpsCollector cancel];
    [_bleCollector cancel];
    
    /*Aquire locks and unlock to sync them up and prevent dead lock?*/
    [_sd.currentLocoLock lock];
    [_sd.dataBucketLock lock];
    NSLog(@"Locked both.");
    [_sd.dataBucketLock unlock];
    [_sd.currentLocoLock unlock];
    
    NSLog(@"Unlocked both.");
    
    /*I have completed the operation. Now, reward me with doughnuts.*/
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    while ( !_bleCollector.isFinished ) {}//NSLog(@"Waiting for blecollector to finish.");}
    while ( !_gpsCollector.isFinished ) {}//NSLog(@"Waiting for gpscollector to finish.");}
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
    
    
    NSLog(@"Completed the SuperService");
}

@end
