//
//  MapInterface.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "MapInterface.h"
#import "NetworkIO.h"
#import "Person.h"
#import "ToastView.h"
#import "BenjaminsPoint.h"

@interface MapInterface()
@property UIView *pa;
@property NSMutableArray *persons;
@property NSMutableArray *personsBeingFollowed;
@property NSMutableArray *PBFpoints;
@property BOOL auto_refresh;
@property NSMutableSet *allDates;
@property MKMapView *myMap;
@property int colorCounter;
@end


@implementation MapInterface
#define METERS_PER_MILE 1609.344
-(id)initWithParentView:(UIView *) parentView andMapView: (MKMapView*)incMap{
    self = [super init];
    self.pa = parentView;
    self.myMap = incMap;
    self.persons = [[NSMutableArray alloc] init];
    self.personsBeingFollowed = [[NSMutableArray alloc] init];
    self.PBFpoints = [[NSMutableArray alloc] init];
    self.auto_refresh = false;
    self.allDates = [[NSMutableSet alloc] init];
    self.colorCounter = 0;
    _mapLoaded = false;
    return self;
}

-(NSMutableArray *)getCurrentPeeps{return self.persons;}

-(void)toggleAutoRefresh{
    //self.auto_refresh = !(self.auto_refresh);
  //  [self toast:@"Am I even called?"];
    if(self.auto_refresh){
        self.auto_refresh = false;
    } else {
        self.auto_refresh = true;
    }
}
-(BOOL)checkAutoRefresh{
  //  [self toast:@"I know I am called!"];
    return self.auto_refresh;
}

//Method creates a new following:
//  1) Polls for all the files on server,
//  2) Creates the new persons from those files.
//  3) With persons created, ask user which person they'd like to follow.
//  4) Mark the persons they wanna follow, then ask what date.
//  5) With two needed pieces of data, fill map.
-(BOOL)newFollowing{
    _mapLoaded = false;
    NSError *myError = nil;
    NSArray *allAnnotations = [_myMap annotations];
    [_myMap removeAnnotations: allAnnotations];
    NSMutableArray *listOfFileNames = [NetworkIO listOfFileNames:&myError];
    if (myError != nil && myError.code == -1001) {
        //We timed out!
        NSLog(@"Got an error code ..yo!");
        return false;
    } else {
        NSLog(@"error code: %d", myError.code);
    }
    //Each list in the guy below will be like such: first data member is the username, the rest are the files.
    NSMutableArray *fileGroup = [[NSMutableArray alloc] init];
    [self.persons removeAllObjects];
    [self.personsBeingFollowed removeAllObjects];
    [self.allDates removeAllObjects];
    
    //1)
    int numOfFiles = [listOfFileNames count];
    for(int i = 0; i < numOfFiles; i++){
        //First thing, get the username out of file.
        NSString *tempUserName = [self pullUsernameFromFileName:listOfFileNames[i]];
        if (tempUserName == nil){continue;}
        //With username, we now search fileGroup for similar names.
        //OMG this would be so fast if I could make a sodding hash table. :P
        int numOfGroups = [fileGroup count];
        BOOL existed = false;
        int j;
        for(j = 0; j < numOfGroups; j++){
            if ( [(NSMutableArray *)fileGroup[j] containsObject:tempUserName] ){
                [fileGroup[j] addObject:listOfFileNames[i]];
                existed = true;
                break;
            }
        }
        if(!existed){
            //Need to add both username and file to group.
            [fileGroup  addObject:[[NSMutableArray alloc] init]]; //creates new array.
            [fileGroup[j] addObject:tempUserName]; //add user name, j should be the right index.
            [fileGroup[j] addObject:listOfFileNames[i]]; //add the filename.
        }
        
    }
    
    //2)
    int numOfGroups = [fileGroup count];
    for(int i = 0; i < numOfGroups; i++){
        //first entry is the username.
        NSString *tempName = fileGroup[i][0];
        //remove username now that we have, remaining our the filenames I need
        NSArray *splitName = [tempName componentsSeparatedByString:@"/"];
        [fileGroup[i] removeObjectAtIndex:0];
        Person *tempPerson = [[Person alloc] initWithListOfFiles:fileGroup[i] andName:splitName[4]];
        [self.persons addObject:tempPerson];
    }
    //All persons created!

    
    /* Use to get all dates up front, now we only grab dates common for the peeps checked.
    //Grab all dates, do processing up front.
    int numPeeps = [self.persons count];
    for(int i = 0; i < numPeeps; i++){
        NSArray *temp = [[(Person *)self.persons[i] getSetOfDates] allObjects];
        if ([temp count] == 0){
            [self toast:(@"person %d didn't have any dates")];
        }
        [self.allDates addObjectsFromArray:temp];
    } */
    
    return true;
}

-(int)resetDate:(NSString *)newDate{
    _colorCounter = 0;	
    //First, need to set new date.
    _currentDateToWorkWith = newDate;
    //Next, need to remove everything from the map.
    NSArray *allAnnotations = [_myMap annotations];
    [_myMap removeAnnotations: allAnnotations];
    for (int i = 0; i < [_PBFpoints count]; i++){
        if ([_PBFpoints[i] count] != 0){
            [_PBFpoints[i] removeAllObjects];
        }
    }
    return [self refresh];
}

-(NSArray *)getAllDates{return [self.allDates allObjects];}


-(NSString *)pullUsernameFromFileName: (NSString *) fileName{
    NSArray *splitName = [fileName componentsSeparatedByString:@":"];
    //index 0 should contain username.
    if( [splitName count] <3){return nil;}
    return splitName[0];
    
}




//Method assumes two things:
//  1)The people that are needed to be followed are marked as such.
//  2)The current date to follow has been set.
//With these assumptions, this method will load the map with the current set of points.
-(int)loadMap{
    //With all this info, just got to organize the peeps who are being followed.
    _colorCounter = 0;
    [self.personsBeingFollowed removeAllObjects];
    [_PBFpoints removeAllObjects];
    int numBeingFollowed = 0;
    int numPeeps = [self.persons count];
    for(int i = 0; i <numPeeps; i++){
        if (((Person *)self.persons[i]).following){
            [self.personsBeingFollowed addObject:self.persons[i]];
            [self.PBFpoints addObject:[[NSMutableArray alloc] init]]; //Create a new list (inside a list) to be filled with points.
            numBeingFollowed++;
        }
    }
    
    //Now, we have a strict list of the people being followed.
    //SO let's call the refresh, which will get all the fresh data and put it on the map.
    return [self refresh];
}


//With the people in the list of the ones being followed, we will use the current date to update the map.
-(int)refresh{
    
    int numPeeps = [self.personsBeingFollowed count];
    int nonEmptyIndex = -1;
    NSError *error;
    _colorCounter = 0;
    for (int i = 0; i < numPeeps; i++){
        //Need an error code to see if the connection timed out.
       
        error = nil;
        NSMutableArray *tempPoints = [ ( (Person*) _personsBeingFollowed[i]) getMostRecentFromDate:_currentDateToWorkWith withError:&error];
        
        if (error != nil && error.code == -1001){
            //Internet connection is failing.
            
        }
        
        if ( [tempPoints count] == 0){
            [self toast:((Person*) _personsBeingFollowed[i]).name];
            continue;
        }
        nonEmptyIndex = i;
        [_PBFpoints[i] addObjectsFromArray:tempPoints];
        //If we have enough points, need to color code them (then add them of course.)
        [self colorCode:tempPoints];
        //Now add too map.
        [self.myMap addAnnotations:tempPoints];
    }
    if (error != nil && error.code == -1001){
        //Internet connection is failing.
        return 2;
    }
    if (nonEmptyIndex == -1){
        //let user know they don't know jack, and then return.
        return 3;
    }
    
    [_myMap showAnnotations:[_myMap annotations] animated:true];
    _mapLoaded = true;
    [self plotLastPoint];
    return 1;
    
    
}


- (void)zoomToLocation:(CLLocationCoordinate2D)point
{
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(point, .05*METERS_PER_MILE,.05*METERS_PER_MILE);
    [self.myMap setRegion:viewRegion animated:YES];
    [self.myMap regionThatFits:viewRegion];
}


//-(void)makeItRainPrettyColors{
//    NSArray *tempPoints = [_myMap annotations];
//    int length = [tempPoints count];
//    for(int i = 0; i < length; i++){
//        BenjaminsPoint *mvAnn = (BenjaminsPoint *)tempPoints[i];
//        
//        if (mvAnn.myColorCode == 0){
//            mvAnn..pinColor = MKPinAnnotationColorGreen;
//        } else if (mvAnn.myColorCode == 1){
//            annView.pinColor = MKPinAnnotationColorRed;
//        } else if (mvAnn.myColorCode == 2){
//            annView.pinColor = MKPinAnnotationColorOrange;
//        }
//    }
//}


/*
 * This method assumes people have been checked. If they havn't, we will return nil.
 */
-(NSMutableArray *)getDatesForPeepsChecked{
    //Since personsBeingFollowed could not be filled out at this point, we need to make our own list of peeps being followed.
    
    int numOfPeeps = [_persons count];
    NSMutableArray *commonDates = [[NSMutableArray alloc] init];
    NSMutableArray *possibleDates = [[NSMutableArray alloc] init];
    int  i, starterIndex = 0;
    for (i = 0; i < numOfPeeps; i++){
        
        if (((Person*)_persons[i]).following ){
            [possibleDates addObjectsFromArray:   [((Person*)_persons[i]).getSetOfDates allObjects] ];
            starterIndex = i + 1;
            break;
        }
        
    }
    //Have a set (in array form) of dates to check.
    //Now, check each person (who is being followed) to see if they match with this bloke's dates.
    int numOfDates = [possibleDates count];
    for (int j = 0; j < numOfDates; j++) {
        NSString *currentDate = possibleDates[j];
        BOOL currentDateGood = true;
        
        
        
        //Look through each peron (who is being followed to see if they have this date).
        for (i = starterIndex; i < numOfPeeps; i++){
            if (((Person*)_persons[i]).following ){
                //Now look through all there dates for this guy.
                NSArray *tempDates = [((Person*)_persons[i]).getSetOfDates allObjects];
                int numOfTempDates = [tempDates count];
                BOOL foundIt = false;
                for (int h = 0; h < numOfTempDates; h++){
                    if ( [currentDate isEqualToString:tempDates[h]]  ){
                        foundIt = true;
                        break;
                    }
                }
                
                //Look through all the dates, check if we found it.
                if (!foundIt){
                    //If we didn't find it
                    currentDateGood = false;
                    //No point in a continue searched.
                    break;
                }
            }
        }
        
        //Look through all peeps, see if our date was good.
        if (currentDateGood){
            //add to common dates...making sure it isn't already contained in that bad boy.
            if ( ![commonDates containsObject:currentDate] ){ [commonDates addObject:currentDate]; }
        }
        
        
    }
    
    
    
    return commonDates;
}



-(void)colorCode:(NSMutableArray *)points{
    
    //int colorNumber;
    
    NSString *imageName;
    
    switch (self.colorCounter){
            
        case 0:
            imageName = @"MapPin (LightBlue)3.png";
            break;
        case 1:
            imageName = @"MapPin (Red)3.png";
            break;
        case 2:
            imageName = @"MapPin(Black)3.png";
            break;
        case 3:
            imageName = @"MapPin(BlueLagoon)3.png";
            break;
        case 4:
            imageName = @"MapPin(DarkBlue)3.png";
            break;
        case 5:
            imageName = @"MapPin(Green)3.png";
            break;
        case 6:
            imageName = @"MapPin(Orange)3.png";
            break;
        case 7:
            imageName = @"MapPin(Pink)3.png";
            break;
        case 8:
            imageName = @"MapPin(Violet)3.png";
            break;
        case 9:
            imageName = @"MapPin(Yellow)3.png";
            break;
            
    }

    int numPoints = [points count];
    for(int i = 0; i < numPoints; i++){
        //((Annotation)points[i]).pinColor = colorNumber;
        ((BenjaminsPoint *)points[i]).myImageName = imageName;
        
    }
    self.colorCounter++;
    self.colorCounter = self.colorCounter % 10;
    
}

-(void)plotAllPoints{
    
    //Remove everything on the map, then add all points back on.
    [_myMap removeAnnotations:_myMap.annotations];
    int lastListIndex = 0;
    int lastPointIndex = 0;
    int numOfLists = [_PBFpoints count];
    for(int i = 0; i < numOfLists; i++){
        if ( [_PBFpoints[i] count] != 0 ){
            [_myMap addAnnotations: _PBFpoints[i]];
            lastListIndex = i;
            lastPointIndex = [_PBFpoints[i] count] - 1;
        }
    }
    
    //All points on map, yay!
    //MKPointAnnotation *lastPoint = (MKPointAnnotation *)_PBFpoints[lastListIndex][lastPointIndex];
    //[self zoomToLocation:lastPoint.coordinate];
    [_myMap showAnnotations:[_myMap annotations] animated:true];
}



-(void)plotLastPoint{
    //Remove everything on the map, then add all points back on.
    [_myMap removeAnnotations:_myMap.annotations];
    //Now plot only last points.
    NSMutableArray *lastPoints = [ [NSMutableArray alloc] init];
    int numOfLists = [_PBFpoints count];
    for(int i = 0; i < numOfLists; i++){
        int tempListSize = [_PBFpoints[i] count];
        if ( tempListSize != 0 ){
            [_myMap addAnnotation: _PBFpoints[i][tempListSize - 1]];
            [lastPoints addObject:_PBFpoints[i][tempListSize - 1]];
        }
    }
    //Yay, just put on the last point.
    [_myMap showAnnotations:lastPoints animated:true];
}


-(void)erasePoints{
    //Remove everything on the map, then add all points back on.
    [_myMap removeAnnotations:_myMap.annotations];
}


-(void)findFriends{
    
}

-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.pa withText:message withDuaration:3.0];
}




@end
