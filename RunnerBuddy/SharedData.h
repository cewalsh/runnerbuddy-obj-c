//
//  SharedData.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/29/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface SharedData : NSObject
@property NSLock *currentLocoLock;
@property NSLock *dataBucketLock;
@property CLLocation *pleaseWork;
@property NSMutableArray *dataBucket;
@end
