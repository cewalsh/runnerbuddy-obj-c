//
//  BeepForBLE.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BeepForBLE : NSOperation <CLLocationManagerDelegate> {
    BOOL executing;
    BOOL finished;
}

-(void)completeOperation;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;

-(id)initWithBeacons:(NSMutableArray *)beaconsToListenFor andLock:(NSLock*) incLock;
-(void)allowVideoTriggering:(BOOL)flag;
-(void)allowSound:(BOOL)flag;
@end
