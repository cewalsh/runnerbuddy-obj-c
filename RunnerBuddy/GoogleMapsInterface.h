//
//  GoogleMapsInterface.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 1/18/16.
//  Copyright © 2016 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMaps;
@interface GoogleMapsInterface : NSObject
//With this init, we can control what we need to on the UI (hopefully)
//  may need to pass in the Map Object if I can't yank it from the ParentView from here.
-(id)initWithParentView:(UIView *) parentView andMapView: (GMSMapView *)incMap;
@property NSString *currentDateToWorkWith;
@property BOOL mapLoaded;
//Provide all trigger options, void no returns, mutate the map.
-(BOOL)newFollowing;
-(int)resetDate:(NSString *)newDate;
-(void)plotAllPoints;
-(void)plotLastPoint;
-(void)erasePoints;
-(void)findFriends;
-(void)toggleAutoRefresh;
-(BOOL)checkAutoRefresh;
-(int)loadMap;
-(int)refresh;
-(NSMutableArray *)getDatesForPeepsChecked;

-(NSMutableArray *)getCurrentPeeps;
-(NSArray *)getAllDates;
@end
