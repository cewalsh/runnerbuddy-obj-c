//
//  ViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/3/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "ViewController.h"
#import "ToastView.h"
#import "RunnerModeInterface.h"
#import "SuperService.h"
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *runnerModeButton;
@property (weak, nonatomic) IBOutlet UIButton *spectatorModeButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property NSOperationQueue *opsQueue;
@property BOOL rrhOn;
@property SuperService *superService;
@property RunnerModeInterface *RMI;
@property BOOL RunnerModeOn;
@end

@implementation ViewController

- (IBAction)unwindToMainMenu:(UIStoryboardSegue *)segue{
    //Do nothing
}

-(void)runnerModeButtonListener{
    //[self toast:@"Prepare for lift off!"];
    
    if (!self.RunnerModeOn){
        [self.RMI startRunnerMode];
        self.RunnerModeOn = true;
        //Change button.
        UIImage * buttonImage = [UIImage imageNamed:@"Runner_Toggle.png"];
        [self.runnerModeButton setImage:buttonImage forState:UIControlStateNormal];
    } else {
        [self.RMI stopRunnerMode];
        self.RunnerModeOn = false;
        UIImage * buttonImage = [UIImage imageNamed:@"Runner.png"];
        [self.runnerModeButton setImage:buttonImage forState:UIControlStateNormal];
    }
}
-(void)spectatorModeButtonListener{
    [self toast:@"Close your eyes and imagine a cool map.\n Now imagine there are points \n displayed on said map.\n Nice, right."];
}
-(void)historyButtonListener{
    /*Re-purposed for record runners heard.*/
    if (!_rrhOn){
        /*Turn it on!*/
        _rrhOn = true;
        _superService = [[SuperService alloc] init];
        [_opsQueue addOperation:_superService];
        
    } else {
        _rrhOn = false;
        /*Turn it off!*/
        [_superService cancel];
        _superService = nil;
    }
    
}
-(void)settingsButtonListener{
    [self toast:@"One day this will go to the \nsettings. But, not today."];
}





-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.view withText:message withDuaration:3.0];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.'
    
    //Here, we will init our buttons. Or ...so I hope.
    [self.runnerModeButton addTarget:self
                              action:@selector(runnerModeButtonListener)
                    forControlEvents:UIControlEventTouchUpInside];
 
    [self.spectatorModeButton addTarget:self
                              action:@selector(spectatorModeButtonListener)
                    forControlEvents:UIControlEventTouchUpInside];
    [self.historyButton addTarget:self
                              action:@selector(historyButtonListener)
                    forControlEvents:UIControlEventTouchUpInside];
    [self.settingsButton addTarget:self
                              action:@selector(settingsButtonListener)
                    forControlEvents:UIControlEventTouchUpInside];
    
    _opsQueue = [[NSOperationQueue alloc] init];
    _rrhOn = false;
   // [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background1.png"]]];
//    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background2.png"]];
//    backgroundView.hidden = TRUE;
//    [self.view addSubview:backgroundView];
    
    self.RunnerModeOn = false;
    //Allocated my runnerModeInerface
    self.RMI = [[RunnerModeInterface alloc] init];
    [self.RMI RMI_Constructor:self.view];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
