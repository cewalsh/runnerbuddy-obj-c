//
//  BenjaminsPoint.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <MapKit/MapKit.h>
@import GoogleMaps;
@interface BenjaminsPoint : NSObject<MKAnnotation> {
    
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    
}

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subtitle;
@property NSInteger time;
@property NSString *myImageName;
@property UIImage *markerColor;
@end
