//
//  BenjaminsDate.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/9/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "BenjaminsDate.h"

@implementation BenjaminsDate
-(NSComparisonResult)compare:(BenjaminsDate *)otherObject{
    
    return [otherObject.theDate compare:_theDate];
    //return [self.theDate compare:otherObject.theDate];
    
}
@end
