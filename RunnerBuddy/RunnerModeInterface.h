//
//  RunnerModeInterface.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface RunnerModeInterface : NSObject <CLLocationManagerDelegate>
//Define the public methods that may be called.
-(void)startRunnerMode;
-(void)stopRunnerMode;
-(void)RMI_Constructor:(UIView *) parentView;
//Tha tha tha tha th th th that's all folks!
@end
