//
//  PairWithBeaconTableViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "PairWithBeaconTableViewController.h"
#import "PairingBeacon.h"
#import "FileIO.h"
#import "PairingTableViewCell.h"
#define BeaconFileName @"beaconMemory"
#define TooLong 10.0
#define longEnough 5.0

@interface PairWithBeaconTableViewController () 
@property (weak, nonatomic) IBOutlet UINavigationItem *doneButton;
@property NSMutableArray *oldBeacons;
@property NSTimeInterval lastTime;
@property UITextField *friendlyNameTextBox;
@property NSIndexPath *indexOfFriendlyNameChange;
@end

@implementation PairWithBeaconTableViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    _beacons = [[NSMutableArray alloc]init];
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [self.tableView addGestureRecognizer:longPressRecognizer];
    _oldBeacons = [[NSMutableArray alloc] init];
    [_pairedBeaconsLock lock];
    long pairedSize = [_pairedBeacons count];
    for (int i = 0; i < pairedSize; i++){
        PairingBeacon *new = [[PairingBeacon alloc] init];
        PairingBeacon *temp = (PairingBeacon *)_pairedBeacons[i];
        new.uuid = temp.uuid;
        new.majorValue = temp.majorValue;
        new.minorValue = temp.minorValue;
        new.rssi = temp.rssi;
        new.timeLastHeard = temp.timeLastHeard;
        new.heardRecently = temp.heardRecently;
        new.pairedWith = temp.pairedWith;
        new.friendlyName = temp.friendlyName;
        new.fresh = false;
        [_beacons addObject:new];
    }
    [_pairedBeaconsLock unlock];
    [self getOldBeacons];
    [self setUpBLEListening];
    _lastTime = 0.0;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


-(void)setUpBLEListening{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    [_locationManager requestAlwaysAuthorization];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
    _myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.proobject.testregion"];
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
    
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_beacons count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PairingTableViewCell *cell = (PairingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PairingCell"];
    
    if (cell == nil){
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PairingTableViewCell" owner:nil options:nil];
        for (id currentObject in topLevelObjects){
            
            if ([currentObject isKindOfClass:[PairingTableViewCell class]]){
                cell = (PairingTableViewCell *)currentObject;
                break;
            }
        }
    }

    PairingBeacon *temp = (PairingBeacon *)_beacons[indexPath.row];
    if (cell.majorLabel == nil){
        cell.majorLabel = [[UILabel alloc] init];
    }
    cell.majorLabel.text = [NSString stringWithFormat:@"Major Value: %@    Minor Value: %@",temp.majorValue, temp.minorValue];
;
    
    cell.friendlyName.adjustsFontSizeToFitWidth = true;
    cell.friendlyName.numberOfLines = 1;
    cell.friendlyName.text = [NSString stringWithFormat:@"%@",temp.friendlyName];
    
   if (temp.pairedWith) {
       UIImage * eyeImage = [UIImage imageNamed:@"Cell Eye Button (Blue) smaller.png"];
       cell.pairingImage.image = eyeImage;
   } else {
       UIImage * eyeImage = [UIImage imageNamed:@"Cell Eye Button (grayed) smaller.png"];
       cell.pairingImage.image = eyeImage;
   }
    
    if (temp.heardRecently){
        /*Flashing*/
        UIImageView *animatedImageView = cell.proximityImage;
        animatedImageView.animationImages = [NSArray arrayWithObjects:
                                             [UIImage imageNamed:@"RB Cell button On.png"],
                                             [UIImage imageNamed:@"RB Cell Button Off.png"],nil];
        animatedImageView.animationDuration = 1.0f;
        animatedImageView.animationRepeatCount = 15;
        [animatedImageView startAnimating];
    } else {
        /*Not flashing.*/
        UIImage * offImage = [UIImage imageNamed:@"RB Cell button On.png"];
        cell.proximityImage.image = offImage;
    }
    
    return cell;
    
    
//    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
//    
//    double timeSinceHeard = (currentTime - temp.timeLastHeard) * 1000;
//    
//    if (timeSinceHeard > TooLong) { temp.heardRecently = false; }
//    else { temp.heardRecently = true; }
//    // Configure the cell...
//    NSString *title  = [NSString stringWithFormat:@"Friendly Name: %@\nUUID: %@\nMajor: %@\tMinor: %@\nRSSI: %ld\nHeard: %@",temp.friendlyName, temp.uuid, temp.majorValue, temp.minorValue,temp.rssi, ( (temp.heardRecently) ? @"Recently" : @"Not Recently" )];
//    cell.textLabel.text = title;
//    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
//    cell.textLabel.numberOfLines = 0;  // 0 means no max.
//    

//    
//    
//    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    PairingBeacon *tappedItem = _beacons[indexPath.row];
    tappedItem.pairedWith = !tappedItem.pairedWith;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 98;
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:self.tableView];
    
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
    if (indexPath == nil) {
        NSLog(@"long press on table view but not on a row");
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"long press on table view at row %ld", (long)indexPath.row);
    } else if ((gestureRecognizer.state == UIGestureRecognizerStateEnded) ){
        /*Display an alert for a name.*/
        _indexOfFriendlyNameChange = indexPath;
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Friendly Name" message:@"Please enter a friendly name:" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        _friendlyNameTextBox = [alert textFieldAtIndex:0];
        _friendlyNameTextBox.keyboardType = UIKeyboardTypeAlphabet;
        _friendlyNameTextBox.placeholder = @"Enter a friendly name!";
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    ((PairingBeacon *)_beacons[_indexOfFriendlyNameChange.row]).friendlyName = _friendlyNameTextBox.text;
    [self.tableView reloadRowsAtIndexPaths:@[_indexOfFriendlyNameChange] withRowAnimation:UITableViewRowAnimationTop];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



- (IBAction)doneButtonListener:(id)sender {
    /*Set our paired beacons.*/
    [_pairedBeaconsLock lock];
    BOOL pairedWithAtLeastOne = false;
    long size = [_beacons count];
    for (int i = 0; i < size; i++){
        PairingBeacon *temp = (PairingBeacon *)_beacons[i];
        if (temp.pairedWith){
            /*Check to see if already has this beacon.*/
            if ([_pairedBeacons indexOfObject:temp] == NSNotFound){
                [_pairedBeacons addObject:temp];
               
            }
            pairedWithAtLeastOne = true;
        } else {
            /*need to remove if it's in there.*/
            long index = [_pairedBeacons indexOfObject:temp];
            if (index != NSNotFound){
                [_pairedBeacons removeObjectAtIndex:index];
                pairedWithAtLeastOne = true;
            }
        }
    }
    [_pairedBeaconsLock unlock];
    if (!pairedWithAtLeastOne){
        /*Let user know that they should at least pair with one guy.*/
        [self displayMessage:@"Hey bud, you didn't pair with any beacons! If don't want to, click the back button instead of clicking done. Thanks!" withTitle:@"Oops!"];
        return;
    }
    [self performSegueWithIdentifier:@"unwindingToMyHome" sender:self];
}





/*Down below are the method needed from the BLE delegate.*/

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region{
        /*Okay, get a bunch of beacons, need to look at our listen and see if we already have them.*/
        /*If we don't add, if we do update important information. etc.*/
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    
    if (_lastTime != 0.0){
        
    } else {
        _lastTime = currentTime;
    }
    
    /*Set everyone to not recently heard*/
    for (int i = 0; i < [_beacons count]; i++){
        ((PairingBeacon *)_beacons[i]).heardRecently = false;
    }
    
    
    
    long numNewBeacons = [beacons count];
    for (int i = 0; i < numNewBeacons; i++){
        PairingBeacon *pbTemp = [[PairingBeacon alloc] init];
        /*Get a temp, new beacons for comparison.*/
        CLBeacon *temp = (CLBeacon *)beacons[i];
        NSString *majorValue = [NSString stringWithFormat:@"%4lX",
                                (unsigned long)[temp.major integerValue]];
        NSString *minorValue =[NSString stringWithFormat:@"%4lX",
                               (unsigned long)[temp.minor integerValue]];
        NSString *uuid = [temp.proximityUUID.UUIDString stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        minorValue = [minorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        majorValue = [majorValue stringByReplacingOccurrencesOfString:@" " withString:@"0"];
        
        
        pbTemp.uuid = [NSString stringWithFormat:@"%@%@%@", uuid,majorValue,minorValue];
        pbTemp.majorValue = majorValue;
        pbTemp.minorValue = minorValue;
        pbTemp.rssi = (temp.rssi == 0) ? -100 : temp.rssi;
        pbTemp.timeLastHeard = currentTime;
        pbTemp.heardRecently = true;
        pbTemp.pairedWith = false;
        pbTemp.friendlyName = @"<Long press to give beacon a friendly name>";
        /*Now, see if we have it.*/
        long indexOfpbTemp = [_beacons indexOfObject:pbTemp];
        /*Check to see if it was even in there.*/
        if (indexOfpbTemp == NSNotFound){
            /*Add to the list.*/
            [_beacons addObject:pbTemp];
        } else {
            /*Did find it, update info.*/
            ((PairingBeacon *)_beacons[indexOfpbTemp]).timeLastHeard = currentTime;
            ((PairingBeacon *)_beacons[indexOfpbTemp]).heardRecently = true;
            ((PairingBeacon *)_beacons[indexOfpbTemp]).rssi = pbTemp.rssi;
        }
        
        
    }
    [self organizeData];
    /*All info is updated, now need to reload the table.*/
    [self.tableView reloadData];
    
}


/*
 * Organize the beacons via two props, recently heard and
 */
-(void)organizeData{
    /*First, organize by followed or not.*/
    long size = [_beacons count];
//    for (int i = 0; i < size; i++){
//        /*If temp is paired with, then move it too the front.*/
//        PairingBeacon *temp = (PairingBeacon *)_beacons[i];
//        if (temp.pairedWith){
//            [_beacons removeObjectAtIndex:i];
//            /*Insert at front.*/
//            [_beacons insertObject:temp atIndex:0];
//        }
//    }
    NSMutableArray *heardRecently = [[NSMutableArray alloc] init];
    NSMutableArray *notHeardRecently = [[NSMutableArray alloc] init];
    /*Next organize by recently heard.*/
    for (int i = 0; i < size; i++){
        /*If temp is not heard recently, then move it too the rear.*/
        PairingBeacon *temp = (PairingBeacon *)_beacons[i];
        if (temp.heardRecently){
            if (!temp.fresh) {
                [heardRecently insertObject:temp atIndex:0];
                temp.fresh = true;
            } else {
                [heardRecently addObject:temp];
            }
        } else {
            temp.fresh = false;
            [notHeardRecently addObject:temp];
        }
    }
    
    [heardRecently sortUsingSelector:@selector(compare:)];
    [notHeardRecently sortUsingSelector:@selector(compare:)];
    [_beacons removeAllObjects];
    [_beacons addObjectsFromArray:heardRecently];
    [_beacons addObjectsFromArray:notHeardRecently];
    
    
}


/*
 * Regardless of whether the user pressed done or back, we will want to save all the beacons
 * that we heard. We'll call the saving method from inside the prepare for segue.
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
    [self saveBeacons];
}

/*
 * This method saves the list of _beacons for consumption when the view is re-open.
 */
-(void)saveBeacons{
    /*First, create string from the beacons.*/
    long size = [_beacons count];
    NSMutableString *data = [NSMutableString stringWithFormat:@""];
    for (int i = 0; i < size; i++){
        PairingBeacon *temp = (PairingBeacon *)_beacons[i];
        [data appendFormat:@"%@,", temp.uuid];
        [data appendFormat:@"%@,", temp.friendlyName];
        [data appendFormat:@"%@,", temp.majorValue];
        [data appendFormat:@"%@,", temp.minorValue];
        [data appendFormat:@"%ld,", temp.rssi];
        [data appendFormat:@"%2.2f,", temp.timeLastHeard];
        [data appendFormat:@"%d,", false];      //temp.heardRecently];
        [data appendFormat:@"%d,\n", temp.pairedWith];
    }
    
    [FileIO saveFileToDevice:[NSMutableString stringWithFormat:BeaconFileName] :[NSString stringWithFormat:@"%@",data]];
}

/*
 * Retrieves the any of the previsouly heard beacons!
 */
-(void)getOldBeacons{
    NSString *fileContents = [FileIO getDataFromSavedFile:BeaconFileName];
    /*First, check to make sure our contents are legit...yo*/
    if (fileContents == nil || [fileContents isEqualToString:@""]){
        return;
    }
    
    /*Valid, so power through them!*/
    NSArray *splitContents = [fileContents componentsSeparatedByString:@"\n"];
    long size = [splitContents count];
    for (int i = 0; i < size; i++){
        NSArray *line = [splitContents[i] componentsSeparatedByString:@","];
        if ([line count] < 8){ continue; }
        PairingBeacon *temp = [[PairingBeacon alloc] init];
        temp.uuid = line[0];
        temp.friendlyName = line[1];
        temp.majorValue = line[2];
        temp.minorValue = line[3];
        temp.rssi = [line[4] integerValue];
        temp.timeLastHeard = [line[5] doubleValue];
        temp.heardRecently = ( [line[6] integerValue] == 0 ) ? false : true;
        temp.pairedWith = ( [line[7] integerValue] == 0 ) ? false : true;
        temp.fresh = false;
        if (temp.pairedWith){
            [_pairedBeaconsLock lock];
            if ( [_pairedBeacons indexOfObject:temp] == NSNotFound){
                [_pairedBeacons addObject:temp];
            }
            [_pairedBeaconsLock unlock];
        }
        [_oldBeacons addObject:temp];
    }
    
    /*For now, just add old beacons to regular beacons.*/
    size = [_oldBeacons count];
    for (int i = 0; i < size; i++){
        PairingBeacon *temp = (PairingBeacon *)_oldBeacons[i];
        if ( [_beacons indexOfObject:temp] == NSNotFound){
            [_beacons addObject:temp];
        }
    }
}

- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    // This method fires when the receiver detects that it has entered the
    // defined beacon region. I disabled this so that we start ranging any
    // beacons even before we enter the region.
    //[self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    // This method fires when the device leaves the defined Beacon Region.
    //[self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    //self.statusLabel.text = @"Exited Region";
}


-(void)displayMessage: (NSString *)message withTitle:(NSString*)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
