//
//  PairingTableViewCell.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/10/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PairingTableViewCell : UITableViewCell{
    IBOutlet UIImageView *proximityImage;
    IBOutlet UIImageView *pairingImage;
    IBOutlet UILabel *majorLabel;
    IBOutlet UILabel *friendlyName;
}
@property (strong, nonatomic) IBOutlet UIImageView *proximityImage;

@property (strong, nonatomic) IBOutlet UIImageView *pairingImage;
@property (strong, nonatomic) IBOutlet UILabel *majorLabel;
@property (strong, nonatomic) IBOutlet UILabel *friendlyName;

@end
