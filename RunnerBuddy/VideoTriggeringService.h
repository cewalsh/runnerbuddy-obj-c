//
//  VideoTriggeringService.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 1/8/16.
//  Copyright (c) 2016 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

#import <AssetsLibrary/AssetsLibrary.h>		//<<Can delete if not storing videos to the photo library.  Delete the assetslibrary framework too requires this)
#define CAPTURE_FRAMES_PER_SECOND		20

@interface VideoTriggeringService : NSObject
<AVCaptureFileOutputRecordingDelegate>
{
    BOOL WeAreRecording;
    
    AVCaptureSession *CaptureSession;
    AVCaptureMovieFileOutput *MovieFileOutput;
    AVCaptureDeviceInput *VideoInputDevice;
}

//@property UIViewController *currentView;
//
//@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

- (void) CameraSetOutputProperties;
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position;
- (void)createSession;
- (void)toggleRecording;
-(BOOL)doneRecording;
@end

