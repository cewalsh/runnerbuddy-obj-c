//
//  PairingBeacon.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "PairingBeacon.h"

@implementation PairingBeacon
-(NSComparisonResult)compare:(PairingBeacon *)otherObject{
    
    if (_timeLastHeard > otherObject.timeLastHeard){
        return NSOrderedAscending;
    } else if (_timeLastHeard < otherObject.timeLastHeard){
        return NSOrderedDescending;
    } else {
        return [_friendlyName compare:otherObject.friendlyName];
    }
    
}
- (BOOL)isEqual:(id)anObject{
    PairingBeacon *otherBeacon = (PairingBeacon *)anObject;
    return [_uuid isEqualToString:otherBeacon.uuid];
}
@end
