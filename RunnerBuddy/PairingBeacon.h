//
//  PairingBeacon.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PairingBeacon : NSObject
@property NSString *uuid;
@property NSString *friendlyName;
@property NSString *majorValue;
@property NSString *minorValue;
@property long rssi;
@property NSTimeInterval timeLastHeard;
@property BOOL heardRecently;
@property BOOL pairedWith;
@property BOOL fresh;
@end
