//
//  NetworkIO.h
//      ~Static class that will contain methods for reaching stuff on server. Whoot!
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkIO : NSObject

+(NSString *) getFileFrom: (NSString *)filePath errorCode:(NSError**) errorCodePointer;
+(NSMutableArray *) listOfFileNames:(NSError**) errorCodePointer;
+(void)sendFileToServerFolderName:(NSMutableString *)folder dataToSend:(NSMutableString *)data fileName:(NSMutableString *)fileName error:(NSError **)incError;
@end
