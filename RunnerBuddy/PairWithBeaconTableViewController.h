//
//  PairWithBeaconTableViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PairWithBeaconTableViewController : UITableViewController <CLLocationManagerDelegate>
@property NSMutableArray *pairedBeacons;
@property NSMutableArray *beacons;
@property NSLock *pairedBeaconsLock;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
