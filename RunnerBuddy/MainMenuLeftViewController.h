//
//  MainMenuLeftViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/1/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface MainMenuLeftViewController : UIViewController
@property MainViewController *myPop;
-(void)setToggles;
@end
