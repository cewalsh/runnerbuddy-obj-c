//
//  ChooseDateTableViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapInterface.h"
#import "GoogleMapsInterface.h"
@interface ChooseDateTableViewController : UITableViewController
@property BOOL useGoogleMaps;
@property (nonatomic) MapInterface *mapInterface;
@property (nonatomic) GoogleMapsInterface *gMapInterface;
//Source is used as a way to define the view controller that is segue-ing into this
//controller, the following codes will be used:
//  3 ~ from the change date button in OptionsViewController   | done -> options  | cancel -> options
//  4 ~ from the donebutton in ChoosePersonTableViewController | done -> map view | cancel -> options
//  6 ~ from the donebutton in ChoosePersonTableViewController | done -> map view | cancel -> mapView
//                                                              (fill with points) (unwind, no points)
@property (nonatomic) int source;
@end
