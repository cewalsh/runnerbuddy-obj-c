//
//  MapViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/3/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface MapViewController : UIViewController  <MKMapViewDelegate>
- (IBAction)unwindToMapView:(UIStoryboardSegue *)segue;
- (void)targetMethod:(NSTimer *)timer;
@end
