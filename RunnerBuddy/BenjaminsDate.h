//
//  BenjaminsDate.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/9/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BenjaminsDate : NSObject
@property BOOL checked;
@property NSString *theDate;
@property BOOL goodDate;
-(NSComparisonResult)compare:(BenjaminsDate *)otherObject;
@end
