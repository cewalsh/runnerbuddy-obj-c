//
//  MapViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/3/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "MapViewController.h"
#import "OptionsViewController.h"
#import "MapInterface.h"
#import "ToastView.h"
#import "BenjaminsPoint.h"
#import "ChoosePersonTableViewController.h"
#import "GoogleMapsInterface.h"
@import GoogleMaps;


@interface MapViewController ()
@property (weak, nonatomic) IBOutlet UIButton *plotAllButton;
@property (weak, nonatomic) IBOutlet UIButton *plotLastButton;
@property (weak, nonatomic) IBOutlet UIButton *clearMapButton;
@property (weak, nonatomic) IBOutlet UIButton *findFriendsButton;
@property (strong, nonatomic) IBOutlet MKMapView *runnerMap;

@property (weak, nonatomic) IBOutlet GMSMapView *hopefulGoogleMapsView;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingInd;
@property MapInterface *myMapInterface;
@property GoogleMapsInterface *myGMapInterface;
@property NSTimer *autoRefreshTimer;
@property GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property BOOL useGoogleMaps;
@end

@implementation MapViewController 
- (IBAction)unwindToMapView:(UIStoryboardSegue *)segue{
    //Do nothing
    //check auto refresh.
    [self checkAutoRefresh];
}

-(void)checkAutoRefresh{
    BOOL check;
    if (!_useGoogleMaps){
        check = [_myMapInterface checkAutoRefresh];
    } else {
        check = [_myGMapInterface checkAutoRefresh];
    }
    if ( check ){
        if (_autoRefreshTimer == nil){
            //create a new timer.
            _autoRefreshTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(targetMethod:) userInfo:nil repeats:YES];
        }
    } else {
        if (_autoRefreshTimer != nil){
            //create a new timer.
            [_autoRefreshTimer invalidate];
             _autoRefreshTimer = nil;
        }
       
    }
}

- (void)targetMethod:(NSTimer *)timer{
    if (!_useGoogleMaps){
        [_myMapInterface refresh];
    } else {
        [_myGMapInterface refresh];
    }
}

-(void)plotAllButtonListener{
    //[self toast:@"Plotting every single gosh darn point."];
    if (!_useGoogleMaps) {
        if (_myMapInterface.mapLoaded) [_myMapInterface plotAllPoints];
    } else {
        if (_myGMapInterface.mapLoaded) [_myGMapInterface plotAllPoints];
    }
}
-(void)plotLastButtonListener{
    //[self toast:@"Just one little point"];
    if (!_useGoogleMaps) {
        if (_myMapInterface.mapLoaded) [_myMapInterface plotLastPoint];
    } else {
        if (_myGMapInterface.mapLoaded) [_myGMapInterface plotLastPoint];
    }
}
-(void)clearMapButtonListener{
    //[self toast:@"NIEN ERASE IT ALL"];
    [_loadingInd startAnimating];
    [self performSelector:@selector(delayedCheck) withObject:nil afterDelay:0.01];
   
    //[_loadingIndicator stopAnimating];
    
}
-(void)findFriendsButtonListener{
    //[self toast:@"Hah! No friends for you."];
    //[self.myMapInterface newFollowing];

    
}        // Yes, do something

-(void)delayedCheck{
    
    if (!_useGoogleMaps){
    
        if(_myMapInterface.mapLoaded && ![_myMapInterface refresh]){
            
            [self displayMessage:@"Unable to access the internet, please\nmake sure you are connected and try again." withTitle:@"Oops!"];
        }
    } else {
        if(_myGMapInterface.mapLoaded && ![_myGMapInterface refresh]){
            
            [self displayMessage:@"Unable to access the internet, please\nmake sure you are connected and try again." withTitle:@"Oops!"];
        }
    }
    [_loadingInd stopAnimating];
}

- (void)AlertNewFollowing{
    UIAlertView *alert = [[UIAlertView alloc] init];

    [alert setTitle:@"New Following"];
    [alert setMessage:@"You need to pick the people you are going to follow. Direct you to the people?"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"Yes"];
    [alert addButtonWithTitle:@"No"];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0)
    {
        [_loadingInd startAnimating];

       // mapToNewFollowing
        [self performSelector:@selector(prepMapInterface) withObject:nil afterDelay:0.01];
        
    }
    else if (buttonIndex == 1)
    {
        [self displayMessage:@"Until you choose a following, no points will be displayed on the map. Please navigate via the \"Options\" button to the Map Options and click \"New Following\"" withTitle:@"Notice"];
        // No
    }
}

-(void)toast:(NSString *)message{
    [ToastView showToastInParentView:self.view withText:message withDuaration:3.0];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set to use google maps.
    _useGoogleMaps = true;
    
    
    // Do any additional setup after loading the view.
    
    if (!_useGoogleMaps){
        [self.runnerMap setDelegate:self];
        _hopefulGoogleMapsView.hidden = true;
    } else {
        _runnerMap.hidden = true;
        //use google maps. let's create this bloke.
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:39.160997
                                                                longitude:-76.710598
                                                                     zoom:6];
        _mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
        _mapView_.myLocationEnabled = YES;
        
        //Testing
//        _hopefulGoogleMapsView = _mapView_;
        
    }
    
    //Here, we will init our buttons. Or ...so I hope.
    [self.plotAllButton addTarget:self
                              action:@selector(plotAllButtonListener)
                    forControlEvents:UIControlEventTouchUpInside];
    
    [self.plotLastButton addTarget:self
                                 action:@selector(plotLastButtonListener)
                       forControlEvents:UIControlEventTouchUpInside];
    [self.clearMapButton addTarget:self
                           action:@selector(clearMapButtonListener)
                 forControlEvents:UIControlEventTouchUpInside];
    [self.findFriendsButton addTarget:self
                            action:@selector(findFriendsButtonListener)
                  forControlEvents:UIControlEventTouchUpInside];
    
    if (!_useGoogleMaps){
        self.myMapInterface = [[MapInterface alloc] initWithParentView:self.view andMapView:_runnerMap];
    } else {
        _myGMapInterface = [[GoogleMapsInterface alloc] initWithParentView:self.view andMapView:_hopefulGoogleMapsView];
    }
    [self AlertNewFollowing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//My fancy prepareForSegue...that parades each day! at sunset and vine..
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if (_autoRefreshTimer != nil){
        [_autoRefreshTimer invalidate];
        _autoRefreshTimer = nil;
    }
    
    if([segue.identifier isEqualToString:@"mapToOptions"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        OptionsViewController *controller = (OptionsViewController *)navController.topViewController;
        
        if (!_useGoogleMaps){
            controller.mapInterface = self.myMapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.myGMapInterface;
            controller.useGoogleMaps = true;
        }
        
    } else if ([segue.identifier isEqualToString:@"mapToNewFollowing"]){
        
            UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
            ChoosePersonTableViewController *controller = (ChoosePersonTableViewController *)navController.topViewController;
        
        if (!_useGoogleMaps){
            controller.mapInterface = self.myMapInterface;
            controller.useGoogleMaps = false;
        } else {
            controller.gMapInterface = self.myGMapInterface;
            controller.useGoogleMaps = true;
        }
        
        
        controller.source = 5;
        
    }
}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
//{
//    
//   /* MKAnnotationView *pinView = [self returnPointView:annotation.coordinate andTitle:annotation.title andImageName:((BenjaminsPoint*)annotation).myImageName];
//    
//    return pinView; */ //Old way
//    MKAnnotationView *pinView = nil;
//    
//    NSString *defaultPinID = [self whichIdentifer:((BenjaminsPoint*)annotation).myImageName];
//    pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
//    if ( pinView == nil ) {pinView = [[MKAnnotationView alloc]
//                                      initWithAnnotation:annotation reuseIdentifier:defaultPinID];}
//    pinView.canShowCallout = YES;
//    pinView.image = [UIImage imageNamed:((BenjaminsPoint*)annotation).myImageName];
//    pinView.annotation = annotation;
//    pinView.centerOffset = CGPointMake(0, -pinView.frame.size.height / 2);
//    return pinView;
//}

-(NSString *) whichIdentifer:(NSString *)id{
    
    if ( [id isEqualToString:@"MapPin (LightBlue)3.png"]){
        return @"lightBluePin";
    } else  if ( [id isEqualToString:@"MapPin (Red)3.png"]){
        return @"redPin";
    } else  if ( [id isEqualToString:@"MapPin(Black)3.png"]){
        return @"blackPin";
    } else  if ( [id isEqualToString:@"MapPin(BlueLagoon)3.png"]){
        return @"blueLagoonPin";
    } else  if ( [id isEqualToString:@"MapPin(DarkBlue)3.png"]){
        return @"darkBluePin";
    } else  if ( [id isEqualToString:@"MapPin(Green)3.png"]){
        return @"greenPin";
    } else  if ( [id isEqualToString:@"MapPin(Orange)3.png"]){
        return @"orangePin";
    } else  if ( [id isEqualToString:@"MapPin(Pink)3.png"]){
        return @"pinkPin";
    } else  if ( [id isEqualToString:@"MapPin(Violet)3.png"]){
        return @"violetPin";
    } else {
        return @"yellowPin";
    }
    
}




//-(MKAnnotationView*) returnPointView: (CLLocationCoordinate2D) location andTitle: (NSString*) title andImageName: (NSString*) imageName{
//    /*Method that acts as a point-generating machine. Takes the parameters of the location, the title, and the color of the
//     pin, and it returns a view that holds the pin with those specified details*/
//    
//    MKPointAnnotation *resultPin = [[MKPointAnnotation alloc] init];
//    MKPinAnnotationView *result = [[MKPinAnnotationView alloc] initWithAnnotation:resultPin reuseIdentifier:Nil];
//    [resultPin setCoordinate:location];
//    resultPin.title = title;
//    result.canShowCallout = YES;
//    //[self toast:title];
//    result.image = [UIImage imageNamed:imageName];
//    return result;
//}


-(void)prepMapInterface{
    int count = 0;
    BOOL passed;
    if (!_useGoogleMaps) {
        passed = [self.myMapInterface newFollowing];
    } else {
        passed = [self.myGMapInterface newFollowing];
    }
    while (!passed && count <3){
        
        if (!_useGoogleMaps) {
            passed = [self.myMapInterface newFollowing];
        } else {
            passed = [self.myGMapInterface newFollowing];
        }
        count++;
    }
    if(passed){
        NSLog(@"OH man it passed...it should not have.");
        [_loadingInd stopAnimating];
        [self performSegueWithIdentifier: @"mapToNewFollowing" sender: self];
        
    } else {
        [self displayMessage:@"Unable to access the internet, please make sure you are connected. To try again, either exit Map View and re-enter, or click \"New Following\" in the Options menu." withTitle:@"Oops!"];
    }
}

-(void)showLoading{

}

-(void)removeLoading{
    
}

-(void)displayMessage: (NSString *)message withTitle:(NSString*)title
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
