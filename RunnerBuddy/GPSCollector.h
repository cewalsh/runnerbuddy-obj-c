//
//  GPSCollector.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/22/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SharedData.h"
@interface GPSCollector : NSOperation <CLLocationManagerDelegate> {
    BOOL executing;
    BOOL finished;
}
-(void)completeOperation;
-(id)initWithSharedData:(SharedData*)sd;
@end
