//
//  MainViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/30/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuperService.h"
#import "BeepForBLE.h"

@interface MainViewController : UIViewController
- (IBAction)unwindToMainMenu:(UIStoryboardSegue *)segue;
@property BOOL listeningToBLE;
@property BOOL listeningForRunners;
@property BOOL videoTriggering;
@property NSMutableArray *pairedBeacons;
@property NSOperationQueue *opsQueue;
@property SuperService *superService;
@property BeepForBLE *beepingService;
@property NSLock *pairedBeaconsLock;
@end
