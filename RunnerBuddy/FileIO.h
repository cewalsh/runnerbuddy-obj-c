//
//  FileIO.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/2/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileIO : NSObject
+(void)saveFileToDevice:(NSMutableString *)fileName :(NSString*)data;
+(NSString*)getDataFromSavedFile:(NSString *)filePath;
+(NSMutableArray*)getSavedFilePaths;
@end
