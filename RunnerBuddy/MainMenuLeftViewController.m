//
//  MainMenuLeftViewController.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/1/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "MainMenuLeftViewController.h"
#import "SuperService.h"
@interface MainMenuLeftViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *listenForBLEToggle;
@property (weak, nonatomic) IBOutlet UISwitch *listenForRunnersToggle;
@property (weak, nonatomic) IBOutlet UISwitch *videoTriggerToggle;

@end

@implementation MainMenuLeftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;

}
- (IBAction)pairWithRunnersButtonListener:(id)sender {
    [_myPop performSegueWithIdentifier:@"mainToPairBeacons" sender:_myPop];
}

-(void)BLEToggle{
    /*Do things.*/
    
    if (_myPop.listeningToBLE == _listenForBLEToggle.on) {
        NSLog(@"Not changing");
        return;
    }
    
    _myPop.listeningToBLE = _listenForBLEToggle.on;
    [self checkBeepForBLEService];
    
}
-(void)RunnersToggle{
    /*Check if in on or off state.*/
    if (_myPop.listeningForRunners == _listenForRunnersToggle.on) {
        NSLog(@"Not changing");
        return;
    }
    
    _myPop.listeningForRunners = _listenForRunnersToggle.on;
    if (_listenForRunnersToggle.on){
        NSLog(@"Runners On!");
        if (nil == _myPop.superService){
            /*Turn on the Listen For Runners Activity.*/
            _myPop.superService = [[SuperService alloc] init];
            @try {
                [_myPop.opsQueue addOperation:_myPop.superService];
            } @catch (NSException *e){
                NSLog(@"oh noes");
            }
        }
    } else {
        NSLog(@"Runners Off!");
        if (_myPop.superService != nil){
            [_myPop.superService cancel];
            while (!_myPop.superService.isFinished ) {}//NSLog(@"Waiting for supserservice to finish.");}
            _myPop.superService = nil;
        }
    }
}

-(void)VideoTriggerToggle{
    /*Check if in on or off state.*/
    if (_myPop.videoTriggering == _videoTriggerToggle.on) {
        NSLog(@"Not changing");
        return;
    }
    
    _myPop.videoTriggering = _videoTriggerToggle.on;
    [self checkBeepForBLEService];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.listenForBLEToggle addTarget:self
                                action:@selector(BLEToggle)
                      forControlEvents:UIControlEventValueChanged];
    [self.listenForRunnersToggle addTarget:self
                                    action:@selector(RunnersToggle)
                          forControlEvents:UIControlEventValueChanged];
    
    [self.videoTriggerToggle addTarget:self
                                    action:@selector(VideoTriggerToggle)
                          forControlEvents:UIControlEventValueChanged];
    
    
}

-(void)setToggles{
    _listenForRunnersToggle.on = _myPop.listeningForRunners;
    _listenForBLEToggle.on = _myPop.listeningToBLE;
   
    _videoTriggerToggle.on = _myPop.videoTriggering;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




//Goal with this method, check our beeping service to see if it should be alive.
// If listen for BLE or video triggering is on, then it should be on.
// If both are off, then it should be off.
-(void)checkBeepForBLEService{
    if (_listenForBLEToggle.on || _videoTriggerToggle.on){
        //if it doesn't already exist, create it.
        if (_myPop.beepingService == nil){
            _myPop.beepingService = [[BeepForBLE alloc] initWithBeacons:_myPop.pairedBeacons andLock:_myPop.pairedBeaconsLock];
            [_myPop.opsQueue addOperation:_myPop.beepingService];
        }
    } else {
        //Niether are on, so get rid of it.
        [_myPop.beepingService cancel];
        _myPop.beepingService = nil;
    }
    // Now, we gotta send some booleans. Each will pass in the right value...hopefully.
    [_myPop.beepingService allowSound:_listenForBLEToggle.on];
    [_myPop.beepingService allowVideoTriggering:_videoTriggerToggle.on];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
