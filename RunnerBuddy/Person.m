//
//  Person.m
//      ~Represents a Person, which includes a name and a collection of Runs associated with the Person.
//      ~This information is obtained via a list of file names (that contain the same username).
//
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/4/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "Person.h"
#import "Run.h"


//List my variables!
@interface Person()
@property NSMutableSet *dates;
@property NSMutableArray *runs;


@end


@implementation Person

-(id) initWithListOfFiles: (NSMutableArray *) listOfFiles andName:(NSString *) userName{
    self = [super init];
    //Now use parameters
    self.runs = [[NSMutableArray alloc] init];
    self.dates = [[NSMutableSet alloc] init];
    self.name = userName;
    self.following = false;
    [self createRuns:listOfFiles];
    return self;
}



-(void)createRuns:(NSMutableArray *) listOfFiles{
    //Create runs!
    
    NSInteger size = [listOfFiles count];
    
    for(int i = 0; i < size; i++){
        //Create temp run, then add to runs.
        NSString *tempFileName = (NSString *)[listOfFiles objectAtIndex:i];
        NSString *tempDate = [self getDateFromFN: tempFileName];
        
        //If either are null, skip to next iteration.
        if (tempFileName == nil || tempDate == nil){
            continue;
        }
        
        //Was going to hash...but objective-c doesn't do this like you'd expect...I mean, let me create an Array of
        // size x, then hash my guys into that array. Nah, either you make a static sized array or one that
        //is filled with nil guys ..which would take FOREVER to fill. okay not forever, simple for loop, but gotta do that every
        //time I create a person. PHOO, nonsense. Like the whole objective-c language.
        [self.dates addObject:tempDate];
        
        
        Run *temp = [[Run alloc] initWithFileName: tempFileName andDate: tempDate];
        temp.owner = _name;
        [self.runs addObject:temp];
        //Repeat until we are done.
        
        
    }
}
-(NSMutableSet*)getSetOfDates{
    return self.dates;
}

-(NSMutableArray*)getMostRecentFromDate:(NSString *)date withError:(NSError**)error{
    NSInteger size = [self.runs count];
    if(size == 0){ return nil;}
    NSMutableArray *toReturn = [[NSMutableArray alloc] init];
    BOOL goodToGo = false;
    for(int i = 0; i < size; i++){
        //Check the run to see if date matches.
        Run *temp = [self.runs objectAtIndex:i];
        if( [temp.date isEqualToString:date]){
            goodToGo = true;
            //Get the data from this date.
            NSArray *tempArray = [temp getMostRecent:error];
            //Now add contents of array into our return array. (Unless we can append?)
            [toReturn addObjectsFromArray:tempArray];
            //Repeat with other runs with same date!
        }
        
        
        
    }
    
    if (goodToGo){
        return toReturn;
    } else {
        return nil;
    }
}

//Method will get all the data from the date passed in, if no
//date was found for the date passed in, it will return nil.
//-(NSMutableArray *) getPointsFromDate:(NSString *) date{
//    
//    NSInteger size = [self.runs count];
//    if(size == 0){ return nil;}
//    NSMutableArray *toReturn = [[NSMutableArray alloc] init];
//    BOOL goodToGo = false;
//    for(int i = 0; i < size; i++){
//        //Check the run to see if date matches.
//        Run *temp = [self.runs objectAtIndex:i];
//        if( [temp.date isEqualToString:date]){
//            goodToGo = true;
//            //Get the data from this date.
//            NSMutableArray *tempArray = [temp getRunData];
//            //Now add contents of array into our return array. (Unless we can append?)
//            [toReturn addObjectsFromArray:tempArray];
//            //Repeat with other runs with same date!
//        }
//        
//        
//        
//    }
//    
//    if (goodToGo){
//        return toReturn;
//    } else {
//        return nil;
//    }
//    
//    
//}


-(NSString *)getDateFromFN:(NSString *) fileName{
    
    NSArray *splitString = [fileName componentsSeparatedByString:@":"];
    if ( [splitString count] <3){
        return nil;
    }
    //splitString contains our date at index 1. (hopefully).
    
    
    return (NSString *) [splitString objectAtIndex:1];
}


-(NSComparisonResult)compare:(Person *)otherObject{
    
    return [_name compare:otherObject.name];
    //return [self.theDate compare:otherObject.theDate];
    
}
@end
