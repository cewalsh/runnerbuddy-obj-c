//
//  GPSCollector.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/22/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "GPSCollector.h"
#import "SuperService.h"

@interface GPSCollector ()
#define refreshInterval 5
#define sleepyTime [NSThread sleepForTimeInterval:0.125];
@property NSDate *lastUpdate;
@property NSInteger updateInterval; 
@property CLLocationManager* locationManager;
@property SharedData *sd;
@end


@implementation GPSCollector

-(id)initWithSharedData:(id)sd {
    self = [super init];
    if (self) {
        finished = NO;
        executing = NO;
    }
    _sd = sd;
    self.updateInterval = refreshInterval;
    return self;
}

- (void)start {
    // Always check for cancellation before launching the task.
    if ([self isCancelled])
    {
        // Must move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    [NSThread detachNewThreadSelector:@selector(main) toTarget:self withObject:nil];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)main {
    @try {
        
        // Do the main work of the operation here.
        /*Just kick of listener. looking to see if we should exit.*/
        [self performSelectorOnMainThread:@selector(forMainThread) withObject:nil waitUntilDone:false];
        
        while (![self isCancelled]) {}
        
        [self completeOperation];
    }
    @catch(...) {
        // Do not rethrow exceptions.
    }
}

-(void)forMainThread{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
}

-(void)stopLocation{
    [_locationManager stopUpdatingLocation];
    _locationManager.delegate = nil;
}


- (void)completeOperation {
    [self performSelectorOnMainThread:@selector(stopLocation) withObject:nil waitUntilDone:false];
    sleepyTime
    [_sd.currentLocoLock lock];
    [_sd.currentLocoLock unlock];
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    executing = NO;
    finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
    NSLog(@"Completed GPSCollector");
}


- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished {
    return finished;
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{

    if (self == nil) {
        NSLog(@"GPS self is nil");
        return;
    }
    if ( [self isCancelled] ){return;}
    
    //if time elapsed < interval, return
    NSDate *currentTime = [NSDate date];
    
    if (self.lastUpdate == Nil)
    {
        self.lastUpdate = currentTime;
    }
    else if (([currentTime timeIntervalSince1970] - [self.lastUpdate timeIntervalSince1970]) < self.updateInterval)
    {
        return;
    }
    
    self.lastUpdate = currentTime;
    
    CLLocation *loc = [locations lastObject];
    
    
    /*Acquite location lock from SuperService...*/
    [_sd.currentLocoLock lock];
    NSLog(@"GPS collector has loco lock");
    /*Change current loco*/
    _sd.pleaseWork = loc;
    /*Release loco lock*/
    [_sd.currentLocoLock unlock];
    NSLog(@"GPS collector unlock loco lock");
    /*Done with the location, nbd.*/
    
}


@end
