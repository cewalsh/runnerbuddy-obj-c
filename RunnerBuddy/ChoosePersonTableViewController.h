//
//  ChoosePersonTableViewController.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/5/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapInterface.h"
#import "GoogleMapsInterface.h"
@interface ChoosePersonTableViewController : UITableViewController
@property BOOL useGoogleMaps;
@property (nonatomic) MapInterface *mapInterface;
@property (nonatomic) GoogleMapsInterface *gMapInterface;
//Source is used as a way to define the view controller that is segue-ing into this
//controller, the following codes will be used:
//  1 ~ from the newFollowing button in OptionsViewController | done button -> choose date | cancel -> Map options
//  2 ~ from the changepersonsbutton in OptionsViewController | done button -> Map options | cancel -> Map options
//  5 ~ from the alert dialogue in MapViewController          | done button -> choose date | cancel -> Map View
@property (nonatomic)  int source;
@end
