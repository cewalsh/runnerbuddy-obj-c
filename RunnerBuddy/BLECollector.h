//
//  BLECollector.h
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 6/22/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SharedData.h"

@interface BLECollector : NSOperation <CLLocationManagerDelegate> {
    BOOL executing;
    BOOL finished;
}
-(void)completeOperation;
-(id)initWithSharedData:(SharedData *)sd;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end
