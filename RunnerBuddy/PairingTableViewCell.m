//
//  PairingTableViewCell.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 7/10/15.
//  Copyright (c) 2015 ProObject. All rights reserved.
//

#import "PairingTableViewCell.h"

@interface PairingTableViewCell()
@end

@implementation PairingTableViewCell
@synthesize proximityImage;
@synthesize pairingImage;
@synthesize majorLabel;
@synthesize friendlyName;

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
