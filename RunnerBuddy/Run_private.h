//
//  Run_private.h
//  RunnerBuddy
//  Defines the private methods inside of Run.m, this is to expose
//  and make them avaliable to testing frameworks.
//  Created by Benjamin M Yankowski The First on 2/4/16.
//  Copyright © 2016 ProObject. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Run_private : NSObject
-(void) fetchRunData: (NSError **)error;
-(void) convertFileData:(NSString *) fileData;
-(NSInteger) getDateFromTitle:(NSString *) title;


/*These accesors are NOT for public use, just for testing.*/
-(NSString *)getFileName;
-(NSMutableArray *)getCurrentPoints;
-(NSUInteger)getNumOfMostRecentPoints;

@end


