//
//  GoogleMapsInterface.m
//  RunnerBuddy
//
//  Created by Benjamin M Yankowski The First on 1/18/16.
//  Copyright © 2016 ProObject. All rights reserved.
//

#import "GoogleMapsInterface.h"
#import "Person.h"
#import "NetworkIO.h"
#import "BenjaminsPoint.h"

@import GoogleMaps;
@interface GoogleMapsInterface()

@property UIView *pa;
@property NSMutableArray *persons;
@property NSMutableArray *personsBeingFollowed;
@property NSMutableArray *PBFpoints;
@property BOOL auto_refresh;
@property NSMutableSet *allDates;
@property GMSMapView *myMap;
@property int colorCounter;
@end



@implementation GoogleMapsInterface

-(id)initWithParentView:(UIView *) parentView andMapView: (GMSMapView *)incMap{
    self = [super init];
    self.pa = parentView;
    self.myMap = incMap;
    self.persons = [[NSMutableArray alloc] init];
    self.personsBeingFollowed = [[NSMutableArray alloc] init];
    self.PBFpoints = [[NSMutableArray alloc] init];
    self.auto_refresh = false;
    self.allDates = [[NSMutableSet alloc] init];
    self.colorCounter = 0;
    _mapLoaded = false;
    return self;
}

//Method creates a new following:
//  1) Polls for all the files on server,
//  2) Creates the new persons from those files.
//  3) With persons created, ask user which person they'd like to follow.
//  4) Mark the persons they wanna follow, then ask what date.
//  5) With two needed pieces of data, fill map.
-(BOOL)newFollowing{
    _mapLoaded = false;
    NSError *myError = nil;
    
    
    
    /*Need to remove all markers from the map.*/
    [_myMap clear];
    /*Need to remove all markers from the map.*/
    
    
    NSMutableArray *listOfFileNames = [NetworkIO listOfFileNames:&myError];
    if (myError != nil && myError.code == -1001) {
        //We timed out!
        NSLog(@"Got an error code ..yo!");
        return false;
    } else {
        NSLog(@"error code: %d", (int)myError.code);
    }
    //Each list in the guy below will be like such: first data member is the username, the rest are the files.
    NSMutableArray *fileGroup = [[NSMutableArray alloc] init];
    [self.persons removeAllObjects];
    [self.personsBeingFollowed removeAllObjects];
    [self.allDates removeAllObjects];
    
    //1)
    NSUInteger numOfFiles = [listOfFileNames count];
    for(int i = 0; i < numOfFiles; i++){
        //First thing, get the username out of file.
        NSString *tempUserName = [self pullUsernameFromFileName:listOfFileNames[i]];
        if (tempUserName == nil){continue;}
        //With username, we now search fileGroup for similar names.
        //OMG this would be so fast if I could make a sodding hash table. :P
        NSUInteger numOfGroups = [fileGroup count];
        BOOL existed = false;
        int j;
        for(j = 0; j < numOfGroups; j++){
            if ( [(NSMutableArray *)fileGroup[j] containsObject:tempUserName] ){
                [fileGroup[j] addObject:listOfFileNames[i]];
                existed = true;
                break;
            }
        }
        if(!existed){
            //Need to add both username and file to group.
            [fileGroup  addObject:[[NSMutableArray alloc] init]]; //creates new array.
            [fileGroup[j] addObject:tempUserName]; //add user name, j should be the right index.
            [fileGroup[j] addObject:listOfFileNames[i]]; //add the filename.
        }
        
    }
    
    //2)
    NSUInteger numOfGroups = [fileGroup count];
    for(int i = 0; i < numOfGroups; i++){
        //first entry is the username.
        NSString *tempName = fileGroup[i][0];
        //remove username now that we have, remaining our the filenames I need
        NSArray *splitName = [tempName componentsSeparatedByString:@"/"];
        [fileGroup[i] removeObjectAtIndex:0];
        Person *tempPerson = [[Person alloc] initWithListOfFiles:fileGroup[i] andName:splitName[4]];
        [self.persons addObject:tempPerson];
    }
    //All persons created!
    
    
    /* Use to get all dates up front, now we only grab dates common for the peeps checked.
     //Grab all dates, do processing up front.
     int numPeeps = [self.persons count];
     for(int i = 0; i < numPeeps; i++){
     NSArray *temp = [[(Person *)self.persons[i] getSetOfDates] allObjects];
     if ([temp count] == 0){
     [self toast:(@"person %d didn't have any dates")];
     }
     [self.allDates addObjectsFromArray:temp];
     } */
    
    return true;
}

//Helper function for new following.
-(NSString *)pullUsernameFromFileName: (NSString *) fileName{
    NSArray *splitName = [fileName componentsSeparatedByString:@":"];
    //index 0 should contain username.
    if( [splitName count] <3){return nil;}
    return splitName[0];
    
}



//Resets the date with the given date passed in.
-(int)resetDate:(NSString *)newDate{
    _colorCounter = 0;
    //First, need to set new date.
    _currentDateToWorkWith = newDate;
    //Next, need to remove everything from the map.
    
    
    /*Remove markers from the map*/
    [_myMap clear];
    /*Remove markers from map.*/
    
    
    for (int i = 0; i < [_PBFpoints count]; i++){
        if ([_PBFpoints[i] count] != 0){
            [_PBFpoints[i] removeAllObjects];
        }
    }
    return [self refresh];
}

//Plots all the points for all the people that are currently being tracked.
-(void)plotAllPoints{
    //Remove everything on the map, then add all points back on.
    [_myMap clear];
    int lastListIndex = 0;
    int lastPointIndex = 0;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    NSUInteger numOfLists = [_PBFpoints count];
    for(int i = 0; i < numOfLists; i++){
        if ( [_PBFpoints[i] count] != 0 ){
            /*
             * Okay, different story here.
             * We create a marker on the map for google, from a Benjamin point.
             * Yolo hastag Swag 2012
             */
            
            NSMutableArray *tempPoints = _PBFpoints[i];
            // ^that's a list!
            for (int j = 0; j < [tempPoints count]; j++){
                
                BenjaminsPoint *tempPoint = (BenjaminsPoint *)tempPoints[j];
                GMSMarker *tempMarker = [GMSMarker markerWithPosition:tempPoint.coordinate];
                tempMarker.title = tempPoint.title;
                tempMarker.snippet = tempPoint.subtitle;
                
                tempMarker.icon = tempPoint.markerColor;
                
                //Now add to map.
                tempMarker.map = _myMap;
                bounds = [bounds includingCoordinate:tempPoint.coordinate];
            }
            
            
            lastListIndex = i;
            lastPointIndex = [_PBFpoints[i] count] - 1;
        }
    }
    
    //All points should be displayed on the map!
    //set zoom.
    //Build an update.
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [_myMap moveCamera:update];
}

//Plots the last point for each of the people that are currently being
//tracked.
-(void)plotLastPoint{
    //Remove everything on the map, then add all points back on.
    [_myMap clear];
    //Now plot only last points.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    NSMutableArray *lastPoints = [ [NSMutableArray alloc] init];
    NSUInteger numOfLists = [_PBFpoints count];
    for(int i = 0; i < numOfLists; i++){
        NSUInteger tempListSize = [_PBFpoints[i] count];
        if ( tempListSize != 0 ){
            
            BenjaminsPoint *tempPoint = (BenjaminsPoint *)_PBFpoints[i][tempListSize - 1];
            // ^that's a list!
        
                
            
                GMSMarker *tempMarker = [GMSMarker markerWithPosition:tempPoint.coordinate];
                tempMarker.title = tempPoint.title;
                tempMarker.snippet = tempPoint.subtitle;
            
                tempMarker.icon = tempPoint.markerColor;
                
                //Now add to map.
                tempMarker.map = _myMap;
                bounds = [bounds includingCoordinate:tempPoint.coordinate];
            
            
            
            
            [lastPoints addObject:_PBFpoints[i][tempListSize - 1]];
        }
    }
    //All last points should be displayed on the map.
    //set zoom.
    //Build an update.
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [_myMap moveCamera:update];
    
}

//Removes all the points from the map. Invisable I believe.
-(void)erasePoints{
    [_myMap clear];
}

//Finds friends?
-(void)findFriends{
    /*Empty for now*/
}

//Toggles the auto refresh feature to the opposite
//of it's current position.
-(void)toggleAutoRefresh{
    if(self.auto_refresh){
        self.auto_refresh = false;
    } else {
        self.auto_refresh = true;
    }
}

//Checks the autorefresh and returns its current position.
-(BOOL)checkAutoRefresh{
    return self.auto_refresh;
}

//Method assumes two things:
//  1)The people that are needed to be followed are marked as such.
//  2)The current date to follow has been set.
//With these assumptions, this method will load the map with the current set of points.
-(int)loadMap{
    //With all this info, just got to organize the peeps who are being followed.
    _colorCounter = 0;
    [self.personsBeingFollowed removeAllObjects];
    [_PBFpoints removeAllObjects];
    int numBeingFollowed = 0;
    NSUInteger numPeeps = [self.persons count];
    for(int i = 0; i <numPeeps; i++){
        if (((Person *)self.persons[i]).following){
            [self.personsBeingFollowed addObject:self.persons[i]];
            [self.PBFpoints addObject:[[NSMutableArray alloc] init]]; //Create a new list (inside a list) to be filled with points.
            numBeingFollowed++;
        }
    }
    
    //Now, we have a strict list of the people being followed.
    //SO let's call the refresh, which will get all the fresh data and put it on the map.
    return [self refresh];
}


//With the people in the list of the ones being followed, we will use the current date to update the map.
-(int)refresh{
    NSUInteger numPeeps = [self.personsBeingFollowed count];
    int nonEmptyIndex = -1;
    NSError *error;
    _colorCounter = 0;
    for (int i = 0; i < numPeeps; i++){
        //Need an error code to see if the connection timed out.
        
        error = nil;
        NSMutableArray *tempPoints = [ ( (Person*) _personsBeingFollowed[i]) getMostRecentFromDate:_currentDateToWorkWith withError:&error];
        
        if (error != nil && error.code == -1001){
            //Internet connection is failing.
            
        }
        
        if ( [tempPoints count] == 0){
//            [self toast:((Person*) _personsBeingFollowed[i]).name];
            continue;
        }
        nonEmptyIndex = i;
        [_PBFpoints[i] addObjectsFromArray:tempPoints];
        //If we have enough points, need to color code them (then add them of course.)
        [self colorCode:_PBFpoints[i]];
        //Unlike iOS maps, I don't add to the map until I want them to be displayed.
    }
    if (error != nil && error.code == -1001){
        //Internet connection is failing.
        return 2;
    }
    if (nonEmptyIndex == -1){
        //let user know they don't know jack, and then return.
        return 3;
    }
    
    
    _mapLoaded = true;
    [self plotAllPoints];
    return 1;
}

-(void)colorCode:(NSMutableArray *)points{
    
    //int colorNumber;
    
    UIImage *markerColor;
    
    switch (self.colorCounter){
            
        case 0:
//            imageName = @"MapPin (LightBlue)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor lightGrayColor]];
            break;
        case 1:
//            imageName = @"MapPin (Red)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor redColor]];
            break;
        case 2:
//            imageName = @"MapPin(Black)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor blackColor]];
            break;
        case 3:
//            imageName = @"MapPin(BlueLagoon)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor blueColor]];
            break;
        case 4:
//            imageName = @"MapPin(DarkBlue)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor purpleColor]];
            break;
        case 5:
//            imageName = @"MapPin(Green)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor greenColor]];
            break;
        case 6:
//            imageName = @"MapPin(Orange)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor orangeColor]];
            break;
        case 7:
//            imageName = @"MapPin(Pink)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor brownColor]];
            break;
        case 8:
//            imageName = @"MapPin(Violet)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor whiteColor]];
            break;
        case 9:
//            imageName = @"MapPin(Yellow)3.png";
            markerColor = [GMSMarker markerImageWithColor:[UIColor yellowColor]];
            break;
            
    }
    
    NSUInteger numPoints = [points count];
    for(int i = 0; i < numPoints; i++){
        //((Annotation)points[i]).pinColor = colorNumber;
        ((BenjaminsPoint *)points[i]).markerColor = markerColor;
        
    }
    self.colorCounter++;
    self.colorCounter = self.colorCounter % 10;
    
}



/*
 * This method assumes people have been checked. If they havn't, we will return nil.
 */
-(NSMutableArray *)getDatesForPeepsChecked{
    //Since personsBeingFollowed could not be filled out at this point, we need to make our own list of peeps being followed.
    
    NSUInteger numOfPeeps = [_persons count];
    NSMutableArray *commonDates = [[NSMutableArray alloc] init];
    NSMutableArray *possibleDates = [[NSMutableArray alloc] init];
    int  i, starterIndex = 0;
    for (i = 0; i < numOfPeeps; i++){
        
        if (((Person*)_persons[i]).following ){
            [possibleDates addObjectsFromArray:   [((Person*)_persons[i]).getSetOfDates allObjects] ];
            starterIndex = i + 1;
            break;
        }
        
    }
    //Have a set (in array form) of dates to check.
    //Now, check each person (who is being followed) to see if they match with this bloke's dates.
    NSUInteger numOfDates = [possibleDates count];
    for (int j = 0; j < numOfDates; j++) {
        NSString *currentDate = possibleDates[j];
        BOOL currentDateGood = true;
        
        
        
        //Look through each peron (who is being followed to see if they have this date).
        for (i = starterIndex; i < numOfPeeps; i++){
            if (((Person*)_persons[i]).following ){
                //Now look through all there dates for this guy.
                NSArray *tempDates = [((Person*)_persons[i]).getSetOfDates allObjects];
                NSUInteger numOfTempDates = [tempDates count];
                BOOL foundIt = false;
                for (int h = 0; h < numOfTempDates; h++){
                    if ( [currentDate isEqualToString:tempDates[h]]  ){
                        foundIt = true;
                        break;
                    }
                }
                
                //Look through all the dates, check if we found it.
                if (!foundIt){
                    //If we didn't find it
                    currentDateGood = false;
                    //No point in a continue searched.
                    break;
                }
            }
        }
        
        //Look through all peeps, see if our date was good.
        if (currentDateGood){
            //add to common dates...making sure it isn't already contained in that bad boy.
            if ( ![commonDates containsObject:currentDate] ){ [commonDates addObject:currentDate]; }
        }
        
        
    }
    
    
    
    return commonDates;
}

//Gets a list of the current people...being followed?
-(NSMutableArray *)getCurrentPeeps{
    return self.persons;
}

//Get all the possible dates.
-(NSArray *)getAllDates{
    return [self.allDates allObjects];
}

@end
